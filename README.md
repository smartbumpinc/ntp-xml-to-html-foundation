# NTP-XML-HTML-Foundation

- `NTP-Foundation-HTML.xsl` - Main transformation script
- `NTP-Table-to-Image.xsl` - Convert HTML tables to images (PNG)

Video of the process - https://youtu.be/tmsKKKEAGWI

## Instructions:
### XML Transformation ###
- In OxygenXML association `NTP-Foundation-HTML.xsl` with XML file and click 'Apply Transformation'
- In Saxon CMD run `java -jar saxon.jar -s:SOURCE -xsl:XSL -o:OUTPUT` where *SOURCE* is the XML file, *XSL* is the XSL file and *OUTPUT* is the HTML file

### HTML Tables to Images ###
- Open HTML file created by `NTP-Table-to-Image.xsl` in browser and click *Click To Download Tables as Images*
- Browser may ask to allow to download multiple files - accept
- All files will be saved in `Download` directory
- Move to `/img` directory

### Imagemagick ###
- `mogrify -format png -path img -define tiff -resample 72 img/RR*.tif`
- `mogrify -format tif -path img -define tiff -resample 300 img/t-*.png`
- `convert "img/*.png" -resize 250x -set filename:area "%t-thumbnail" img/'%[filename:area].png'`
- `mogrify -crop 250x175+0+0 -path img img/*-thumbnail.png`

### Non-NTP Framework Files ###
- `html/js/override.js`
- `html/css/override.css`