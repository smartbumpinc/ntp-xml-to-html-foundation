$('a.ref').on('click', function() {
      $('#ntp-tabs').foundation('selectTab', 'References');
      });
      
      $(document).ready(function($) {
      $('span.has-tip').each(function(){
      var $this = $(this).attr('tip');
      var newtooltip=$(this).attr('tip');
      var htmltooltip=$("p" + (newtooltip)).text();
      $(this).attr('title', htmltooltip);
      })
      $(document).foundation();
      });
      
      var anchor = window.location.hash;
      tabs = new Array();
      tabset = $('[data-tabs]');
      tablink = tabset.find('li a');
  
  //populate array with tab links
  tablink.each(function(){
      var tab = $(this).attr('href');
      tabs.push(tab);
  });
  
  //prevent default anchor scroll behavior on page load
  if (jQuery.inArray(anchor, tabs) !== -1) {
      tabset.on('deeplink.zf.tabs', function() {
          $('html, body').animate({
              scrollTop: $('#top').offset().top
          }, 0);
      });
  }
  
  //add to browser history on tab interaction
  tablink.each(function(){
      var tab = $(this).attr('href');
          tabIndex = jQuery.inArray(tab, tabs);
      if (tabIndex == 0) {
          var previous = tabs[tabs.length - 1];
          var next = tabs[1];
      } else {
          var previous = tabs[tabIndex - 1];
          var next = tabs[tabIndex + 1];
      }
      $(this).on({
          'click.zf.tabs': function() {
              if (!$(this).parent().hasClass('is-active')) {
                  history.pushState({}, '', tab);
              }
          },
          'keydown.zf.tabs': function(e) {
              Foundation.Keyboard.handleKey(e, 'Tabs', {
                  open: function() {
                      history.pushState({}, '', tab);
                  },
                  previous: function() {
                      history.pushState({}, '', previous);
                  },
                  next: function() {
                      history.pushState({}, '', next);
                  }
              });
          }
      });
  });
  
  //switch tabs on browser history change
  $(window).on('hashchange', function(e) {
      var active = window.location.hash;
          target = tabset.find('a[href="' + active + '"]');
      if (jQuery.inArray(active, tabs) !== -1) {
          tabset.foundation('selectTab', active);
          target.focus();
      }
  });


      