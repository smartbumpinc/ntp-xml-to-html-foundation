<?xml version="1.0"?>
<!-- ============================================================= -->
<!--  PROJECT:   NTP XML-to-HTML v2                                -->
<!--  DATE:      May 2019                                          -->
<!--                                                               -->
<!-- ============================================================= -->

<!-- ============================================================= -->
<!--  SYSTEM:    NCBI Archiving and Interchange Journal Articles   -->
<!--                                                               -->
<!--  PURPOSE:   Provide an HTML preview of a journal article,     -->
<!--             in a form suitable for NTP website.               -->
<!--                                                               -->
<!--  PROCESSOR DEPENDENCIES:                                      -->
<!--             None: standard XSLT 1.0 / 2.0                     -->
<!--             Tested using Saxon 6.5, Saxon 9.4.0.3             -->
<!--                                                               -->
<!--  INPUT:     An XML document valid to (any of) the             -->
<!--             NLM/NCBI JATS Book DTD                            -->
<!--                                                               -->
<!--  OUTPUT:    HTML                                              -->
<!--                                                               -->
<!--  CREATED FOR:                                                 -->
<!--             National Toxicology Program (NTP)                 -->
<!-- ============================================================= -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:mml="http://www.w3.org/1998/Math/MathML"
  xmlns:date="http://exslt.org/dates-and-times" exclude-result-prefixes="xlink mml date">


  <xsl:output doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
    doctype-system="http://www.w3.org/TR/html4/loose.dtd" encoding="UTF-8" indent="yes"/>

  <xsl:strip-space elements="*"/>

  <!-- Space is preserved in all elements allowing #PCDATA -->
  <xsl:preserve-space
    elements="abbrev abbrev-journal-title access-date addr-line
              aff alt-text alt-title article-id article-title
              attrib award-id bold chapter-title chem-struct
              collab comment compound-kwd-part compound-subject-part
              conf-acronym conf-date conf-loc conf-name conf-num
              conf-sponsor conf-theme contrib-id copyright-holder
              copyright-statement copyright-year corresp country
              date-in-citation day def-head degrees disp-formula
              edition elocation-id email etal ext-link fax fpage
              funding-source funding-statement given-names glyph-data
              gov inline-formula inline-supplementary-material
              institution isbn issn-l issn issue issue-id issue-part
              issue-sponsor issue-title italic journal-id
              journal-subtitle journal-title kwd label license-p
              long-desc lpage meta-name meta-value mixed-citation
              monospace month named-content object-id on-behalf-of
              overline p page-range part-title patent person-group
              phone prefix preformat price principal-award-recipient
              principal-investigator product pub-id publisher-loc
              publisher-name related-article related-object role
              roman sans-serif sc season self-uri series series-text
              series-title sig sig-block size source speaker std
              strike string-name styled-content std-organization
              sub subject subtitle suffix sup supplement surname
              target td term term-head tex-math textual-form th
              time-stamp title trans-source trans-subtitle trans-title
              underline uri verse-line volume volume-id volume-series
              xref year

              mml:annotation mml:ci mml:cn mml:csymbol mml:mi mml:mn 
              mml:mo mml:ms mml:mtext"/>


  <xsl:param name="transform" select="'NTP-Foundation-HTML.xsl'"/>

  <xsl:param name="report-warnings" select="'no'"/>

  <xsl:variable name="verbose" select="$report-warnings = 'yes'"/>

  <!-- Keys -->

  <!-- To reduce dependency on a DTD for processing, we declare
       a key to use instead of the id() function. -->
  <xsl:key name="element-by-id" match="*[@id]" use="@id"/>

  <!-- Enabling retrieval of cross-references to objects -->
  <xsl:key name="xref-by-rid" match="xref" use="@rid"/>

  <!-- ============================================================= -->
  <!--  ROOT TEMPLATE - FOUNDATION FRAMEWORK                       -->
  <!-- ============================================================= -->

  <xsl:template match="/">
    <html lang="en">
      <xsl:call-template name="make-html-head"/>
      <body class="ntpLanding">
        <xsl:call-template name="make-header"/>
        <main>
          <nav class="row column" aria-label="You are here:">
            <ul class="breadcrumbs">
              <li><a href="/index.cfm">Home</a></li>
              <xsl:variable name="breadcrumb-title">
                <xsl:value-of select="normalize-space(/book/book-meta/book-title-group/book-title/named-content)"/>
              </xsl:variable>
              <li><xsl:value-of select="$breadcrumb-title"/></li>
            </ul>
          </nav>
          <div class="row large-unstack">
            <article id="pagecontent" class="page-listing area-of-research small-12 columns">
              <xsl:comment>ARTICLE BODY - START</xsl:comment>
              <div class="overview callout blue">
                <div id="share-widget">
                  <form name="search" id="search" class="input-group" method="get" action="https://ntpsearch.niehs.nih.gov/">
                    <span class="input-group-label"><i class="fa fa-search" aria-hidden="true"></i></span><input type="text" placeholder="Search this Report" id="query-search" class="input-group-field queryAutocomplete ui-autocomplete-input" name="query" title="Search this Report" aria-label="Search Keyword" autocomplete="off"/>
                    <input type="submit" class="button input-group-button" value="Search"/>
                  </form>
                  <div id="share">
                    <strong>Share This:</strong>
                    <div class="addthis_sharing_toolbox noprint"> </div>
                  </div>
                  <div id="friendly-url">
                    <span class="copytinyurl" data-clipboard-text="https://ntp.niehs.nih.gov/"
                      title="Copy link to clipboard">https://ntp.niehs.nih.gov/go</span>
                    <button class="copytinyurl" data-clipboard-text="https://ntp.niehs.nih.gov/go/botanical"
                      title="Copy link to clipboard">
                      <i class="fa fa-copy"/>
                    </button>
                  </div>
                  <script src="https://ntp.niehs.nih.gov/js/addthis_widget.js#pubid=ra-552d09fa651a6014" async="async">
                  </script>
                  <div class="float-right img1">
                    <xsl:variable name="file-name">
                      <xsl:value-of select="translate(/book/book-meta/book-id, ' ','')"/>
                    </xsl:variable>
                    <xsl:variable name="alt"><xsl:value-of select="/book/book-meta/book-title-group/book-title/named-content"/></xsl:variable>
                    <img src="img/{$file-name}-thumbnail.png" alt="{$alt}"/>
                  </div>
                </div>
                <xsl:call-template name="title"/>
                <p>
                  <xsl:call-template name="authors"/>
                </p>
                <p>
                  <xsl:call-template name="make-citation-download"/>
                </p>
              </div>
              <xsl:call-template name="tab-nav"/>
              <div class="tabs-content" data-tabs-content="ntp-tabs">
                <xsl:apply-templates/>
              </div>
              <xsl:comment>ARTICLE BODY - END</xsl:comment>
            </article>
          </div>
        </main>
        <xsl:call-template name="make-footer"/>
      </body>
    </html>
  </xsl:template>

  <xsl:template name="make-html-head">
    <head>
      <xsl:comment>ARTICLE METADATA - START</xsl:comment>
      <title>
        <xsl:value-of select="normalize-space(/book/book-meta/book-title-group/book-title/named-content)"/>
      </title>
      <xsl:variable name="meta-description">
        <xsl:value-of select="normalize-space(/book/book-meta/book-title-group/book-title/named-content)"/>
      </xsl:variable>
      <xsl:variable name="publication-date">
        <xsl:value-of select="/book/book-meta/pub-date/month"/>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="/book/book-meta/pub-date/year"/>
      </xsl:variable>
      <xsl:variable name="journal-name">
        <xsl:value-of select="/book/book-meta/publisher/publisher-name/institution-wrap/institution"/>
      </xsl:variable>
      <xsl:variable name="journal-title">
        <xsl:value-of select="/book/collection-meta/title-group/title"/>
      </xsl:variable>
      <meta name="description" content="{$meta-description}"/>
      <meta name="citation_title" content="{$meta-description}"/>
      <meta name="citation_publication_date" content="{$publication-date}"/>
      <meta name="citation_journal_title" content="{$journal-title}"/>
      <xsl:for-each select="/book/book-meta/contrib-group/contrib/name">
        <xsl:variable name="author-given-name">
          <xsl:value-of select="given-names"/>
        </xsl:variable>
        <xsl:variable name="author-surname">
          <xsl:value-of select="surname"/>
        </xsl:variable>
        <meta name="citation_author" content="{$author-surname}, {$author-given-name}"/>
      </xsl:for-each>
      <xsl:comment>ARTICLE METADATA - END</xsl:comment>
      <meta http-equiv="x-ua-compatible" content="ie=edge"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <link rel="shortcut icon" href="https://ntp.niehs.nih.gov/favicon_ntp.ico"/>
      <link rel="stylesheet" type="text/css" media="screen" href="https://ntp.niehs.nih.gov/css/ntpweb.css"/>
      <xsl:comment>ARTICLE OVERRIDE CSS - START</xsl:comment>
      <link rel="stylesheet" type="text/css" media="screen" href="css/override.css"/>
      <xsl:comment>ARTICLE OVERRIDE CSS - END</xsl:comment>
      <link rel="stylesheet" type="text/css" media="print" href="https://ntp.niehs.nih.gov/css/ntpwebprint.css"/>
      <script src="https://ntp.niehs.nih.gov/js/ntpweb-bundle-new.min.js?version=28"/>
    </head>
  </xsl:template>

  <xsl:template name="make-header">
    <div id="skip-nav">
      <div>
        <a href="#main-nav" onclick="getElementById('#main-nav').focus()"
          style="color: rgb(253, 254, 254)">Skip to Main Navigation</a>
      </div>
      <div>
        <a href="#pagecontent" onclick="getElementById('#pagecontent').focus()"
          style="color: rgb(253, 254, 254)">Skip to Page Content</a>
      </div>
    </div>
    <header id="top">
      <div class="usa-banner">
        <div class="usa-accordion">
          <div class="usa-banner-header">
            <div class="usa-grid usa-banner-inner">
              <img src="https://ntp.niehs.nih.gov/images/template/favicon-57.png" alt="U.S. flag"/>
              <p> An official website of the United States government </p>
              <button class="usa-accordion-button usa-banner-button" aria-expanded="false"
                id="gov-banner-button" onclick="toggle('gov-banner', 'gov-banner-button')">
                <span class=" usa-banner-button-text">Here's how you know</span>
              </button>
            </div>
          </div>
          <div class="usa-banner-content usa-accordion-content usa-closed" id="gov-banner">
            <div class="usa-banner-guidance-gov usa-width-one-half">
              <img class="usa-banner-icon usa-media_block-img"
                src="https://ntp.niehs.nih.gov/images/template/dot-gov.svg" alt="Dot gov"/>
              <div class="usa-media_block-body">
                <p>
                  <strong>The .gov means it’s official.</strong>
                  <br/> Federal government websites often end in .gov or .mil. Before sharing
                  sensitive information, make sure you’re on a federal government site. </p>
              </div>
            </div>
            <div class="usa-banner-guidance-ssl usa-width-one-half">
              <img class="usa-banner-icon usa-media_block-img"
                src="https://ntp.niehs.nih.gov/images/template/icon-https.svg" alt="Https"/>
              <div class="usa-media_block-body">
                <p>
                  <strong>The site is secure.</strong>
                  <br/> The <strong>https://</strong> ensures that you are connecting to the
                  official website and that any information you provide is encrypted and transmitted
                  securely. </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="header-nav">
        <div class="row">
          <div class="column"> </div>
          <div class="column shrink">
            <ul class="menu">
              <li>
                <a href="https://ntp.niehs.nih.gov/go/calendar">Calendar<span> &amp;
                  Events</span></a>
              </li>
              <li>
                <a href="https://ntp.niehs.nih.gov/go/newsmedia">News &amp; Media</a>
              </li>
              <li>
                <a href="https://ntp.niehs.nih.gov/go/getinvolved">Get Involved</a>
              </li>
              <li>
                <a href="https://ntp.niehs.nih.gov/go/support">Support</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div id="header-print" class="print-only">
        <img src="https://ntp.niehs.nih.gov/images/template/logo_print.gif"
          alt="National Toxicology Program"/>
      </div>
      <div class="header-identity">
        <div class="row medium-unstack align-middle">
          <div class="columns large-4 medium-6 small-12 small-order-1 header-logo">
            <a href="https://ntp.niehs.nih.gov">
              <img src="https://ntp.niehs.nih.gov/images/template/logo_ntp_white.png"
                title="Return the NTP home page"
                alt="National Toxicology Program - U.S. Department of Health and Human Services"/>
            </a>
          </div>
          <div class="columns large-4 large-offset-4 medium-6 small-order-2 header-search">
            <form name="search" id="search" class="input-group" method="get"
              action="https://ntpsearch.niehs.nih.gov/">
              <span class="input-group-label">
                <i class="fa fa-search" aria-hidden="true"/>
              </span>
              <input type="text" placeholder="Search the NTP Website" id="query-search"
                class="input-group-field queryAutocomplete ui-autocomplete-input" name="query"
                title="Search the NTP Website" aria-label="Search Keyword"/>
              <input type="submit" class="button input-group-button" value="Search"/>
            </form>
          </div>
        </div>
      </div>
      <nav class="row column top-bar" tabindex="-1" id="main-nav"> NAVIGATION </nav>
    </header>

  </xsl:template>
  
  <xsl:template name="get-month-name">
    <xsl:variable name="publication-month" select="/book/book-meta/pub-date/month"/>
    <xsl:choose>
      <xsl:when test="$publication-month = '01'">January </xsl:when>
      <xsl:when test="$publication-month = '02'">February </xsl:when>
      <xsl:when test="$publication-month = '03'">March </xsl:when>
      <xsl:when test="$publication-month = '04'">April </xsl:when>
      <xsl:when test="$publication-month = '05'">May </xsl:when>
      <xsl:when test="$publication-month = '06'">June </xsl:when>
      <xsl:when test="$publication-month = '07'">July </xsl:when>
      <xsl:when test="$publication-month = '08'">August </xsl:when>
      <xsl:when test="$publication-month = '09'">September </xsl:when>
      <xsl:when test="$publication-month = '10'">October </xsl:when>
      <xsl:when test="$publication-month = '11'">November </xsl:when>
      <xsl:when test="$publication-month = '12'">December </xsl:when>
    </xsl:choose>
    
  </xsl:template>

  <xsl:template name="title">
    <xsl:variable name="publisher-name">
      <xsl:value-of select="/book/book-meta/publisher/publisher-name/institution-wrap/institution[@content-type='publication-division']"/>
    </xsl:variable>
    <xsl:variable name="publication-year">
      <xsl:value-of select="/book/book-meta/pub-date/year"/>
    </xsl:variable>
    <xsl:variable name="title-group-title">
      <xsl:value-of select="/book/collection-meta/title-group/title"/>
    </xsl:variable>
    <xsl:variable name="book-id">
      <xsl:value-of select="/book/book-meta/book-id"/>
    </xsl:variable>
    <xsl:variable name="issn">
      <xsl:value-of select="/book/collection-meta/issn"/>
    </xsl:variable>
    <xsl:variable name="institution">
      <xsl:value-of select="/book/book-meta/publisher/publisher-name/institution-wrap/institution[@content-type='publication-institute']"/>
    </xsl:variable>
    <xsl:variable name="department">
      <xsl:value-of select="/book/book-meta/publisher/publisher-name/institution-wrap/institution[@content-type='publication-department']"/>
    </xsl:variable>
    
    <p><strong><xsl:value-of select="$book-id"/> | <xsl:call-template name="get-month-name" /> <xsl:value-of select="$publication-year"/> | ISSN: <xsl:value-of select="$issn"/></strong><br/>
    <xsl:value-of select="$publisher-name"/>, <xsl:value-of select="$institution"/>, <xsl:value-of select="$department"/></p>
    <h1>
      <xsl:value-of
        select="normalize-space(/book/book-meta/book-title-group/book-title/named-content)"/>
    </h1>
  
    
  </xsl:template>

  <xsl:template name="authors">
    <p>
      <xsl:for-each select="/book/book-meta/contrib-group[@content-type = 'authors']">
        <xsl:variable name="collab">
          <xsl:value-of select="contrib/collab"/>
        </xsl:variable>
        <xsl:variable name="csup">
          <xsl:value-of select="contrib/collab/xref"/>
        </xsl:variable>

        <xsl:if test="position() > 1">
          <xsl:choose>
            <xsl:when test="position() = last()"> and </xsl:when>
            <xsl:otherwise>, </xsl:otherwise>
          </xsl:choose>
        </xsl:if>
        <xsl:value-of select="substring($collab, 1, string-length($collab) - 1)"/>
        <sup>
          <xsl:value-of select="$csup"/>
        </sup>
      </xsl:for-each>
    </p>
    <!--<p>
      <xsl:for-each select="/book/book-meta/contrib-group[@content-type = 'collaborators']/contrib">
        <xsl:variable name="lastname">
          <xsl:value-of select="name/surname"/>
        </xsl:variable>
        <xsl:variable name="firstname">
          <xsl:value-of select="name/given-names"/>
        </xsl:variable>
        <xsl:variable name="degree">
          <xsl:value-of select="name/degrees"/>
        </xsl:variable>
        <xsl:variable name="sup">
          <xsl:value-of select="xref"/>
        </xsl:variable>
        <xsl:if test="position() > 1">
          <xsl:choose>
            <xsl:when test="position() = last()"> and </xsl:when>
            <xsl:otherwise>, </xsl:otherwise>
          </xsl:choose>
        </xsl:if>
        <xsl:value-of select="$firstname"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="$lastname"/>
        <sup>
          <xsl:value-of select="$sup"/>
        </sup>
      </xsl:for-each>
    </p>-->
    <p>
          <xsl:for-each select="/book/book-meta/contrib-group[@content-type = 'authors']/contrib">
        <xsl:variable name="lastname">
          <xsl:value-of select="name/surname"/>
        </xsl:variable>
        <xsl:variable name="firstname">
          <xsl:value-of select="name/given-names"/>
        </xsl:variable>
        <xsl:variable name="degree">
          <xsl:value-of select="name/degrees"/>
        </xsl:variable>
        <xsl:variable name="sup">
          <xsl:value-of select="xref"/>
        </xsl:variable>
        <xsl:if test="position() > 1">
          <xsl:choose>
            <xsl:when test="position() = last()"> and </xsl:when>
            <xsl:otherwise>, </xsl:otherwise>
          </xsl:choose>
        </xsl:if>
        <xsl:value-of select="$firstname"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="$lastname"/>
        <sup>
          <xsl:value-of select="$sup"/>
        </sup>
      </xsl:for-each>
    </p>
    <p>
      <ul class="author-accordion" data-accordion="" data-allow-all-closed="true" data-multi-expand="true">
        <li class="accordion-item" data-accordion-item="" aria-expanded="true" aria-selected="true">
          <a href="#" class="author-accordion-title" aria-controls="author-info-accordion" role="tab" id="author-info-col" aria-expanded="false" aria-selected="false">Author Information</a>
          <div class="author-accordion-content" data-tab-content="">
      <xsl:for-each select="/book/book-meta/contrib-group[@content-type = 'authors']/aff">
        <xsl:variable name="aff">
          <xsl:value-of select="substring(., 2, string-length(.) - 1)"/>
        </xsl:variable>
        <xsl:variable name="label">
          <xsl:value-of select="label"/>
        </xsl:variable>
        
          <sup>
            <xsl:value-of select="$label"/>
          </sup>
          <xsl:value-of select="$aff"/>
        <br/>
      </xsl:for-each>
          </div>
        </li>
      </ul>

    </p>
  </xsl:template>

  <xsl:template name="make-citation-download">
    <xsl:variable name="doi">
      <xsl:value-of
        select="/book/front-matter[1]/front-matter-part[4]/named-book-part-body[1]/p[4]/ext-link[1]/@*[namespace-uri()='http://www.w3.org/1999/xlink' and local-name()='href']"
      />
    </xsl:variable>
    <xsl:variable name="doi-citation">
      <xsl:value-of
        select="translate(/book/front-matter[1]/front-matter-part[4]/named-book-part-body[1]/p[4]/ext-link[1]/@*[namespace-uri()='http://www.w3.org/1999/xlink' and local-name()='href'],'https://doi.org/10.22427/','')"
      />
    </xsl:variable>
    <xsl:variable name="file-name">
      <xsl:value-of select="translate(/book/book-meta/book-id, ' ','')"/>
    </xsl:variable>
    <p id="doi"><strong>DOI: </strong><a href="{$doi}"><xsl:value-of select="$doi"/></a></p>
    <p id="citation">
      <strong>Download Citation: </strong><a
        href="{$doi-citation}.ris" title="compatible with EndNote, Reference Manager, ProCite, RefWorks">RIS</a> | <a href="{$doi-citation}.bib" title="compatible with BibDesk, LaTeX">BibTex</a><br/>
    </p>
    <p>
      <a href="https://ntp.niehs.nih.gov/{$file-name}.pdf" class="button small">FULL REPORT</a><xsl:text>&#8198;</xsl:text><xsl:text>&#8198;</xsl:text><xsl:text>&#8198;</xsl:text>
      <a href="https://ntp.niehs.nih.gov/{$file-name}-summary.pdf" class="button small">LAY SUMMARY</a><xsl:text>&#8198;</xsl:text><xsl:text>&#8198;</xsl:text><xsl:text>&#8198;</xsl:text>
    </p>
  </xsl:template>

  <xsl:template name="tab-nav">
    <xsl:variable name="appendix">
      <xsl:value-of
        select="/book/book-body/book-part/back/app/label"/>
    </xsl:variable>
    <div id="tab-nav">
      <ul class="tabs full-width" data-tabs="" data-deep-link="true" id="ntp-tabs">
        <xsl:for-each select="/book/book-meta/abstract/title[text() != '']">
          <xsl:variable name="nav_tab">
            <xsl:value-of select="."/>
          </xsl:variable>
          <li class="tabs-title is-active">
            <a href="#{$nav_tab}" aria-selected="true">
              <xsl:value-of select="$nav_tab"/>
            </a>
          </li>
        </xsl:for-each>
        <xsl:for-each
          select="/book/book-body/book-part/book-part-meta/title-group/title[text() != ''] | /book/book-body/book-part/body/sec/title[text() != '']">
          <xsl:variable name="nav_tab">
            <xsl:value-of select="translate(., ' ', '')"/>
          </xsl:variable>
          <xsl:variable name="nav_name">
            <xsl:value-of select="."/>
          </xsl:variable>
          <li class="tabs-title">
            <a data-tabs-target="{$nav_tab}" href="#{$nav_tab}">
              <xsl:value-of select="$nav_name"/>
            </a>
          </li>
        </xsl:for-each>
        <xsl:for-each select="/book/book-body/book-part/back/app-group/app/sec/title[text() != '']">
          <xsl:variable name="nav_tab">
            <xsl:value-of select="translate(., ' ', '')"/>
          </xsl:variable>
          <xsl:variable name="nav_name">
            <xsl:value-of select="."/>
          </xsl:variable>
          <li class="tabs-title">
            <a data-tabs-target="{$nav_tab}" href="#{$nav_tab}">
              <xsl:value-of select="$nav_name"/>
            </a>
          </li>
        </xsl:for-each>
        <xsl:for-each select="/book/book-back/ref-list/title[text() != ''] | /book/book-body/book-part/back/ref-list/title[text() != '']">
          <xsl:variable name="nav_tab">
            <xsl:value-of select="."/>
          </xsl:variable>
          <li class="tabs-title">
            <a data-tabs-target="{$nav_tab}" href="#{$nav_tab}">
              <xsl:value-of select="$nav_tab"/>
            </a>
          </li>
        </xsl:for-each>
        <xsl:if test="contains($appendix, 'Appendix')">
          <li class="tabs-title">
            <a data-tabs-target="appendix" href="#appendix">Appendix</a>
          </li>
        </xsl:if>
        <xsl:if test="/book/front-matter/front-matter-part">
          <li class="tabs-title">
            <a data-tabs-target="about" href="#about">About this Report</a>
          </li>
        </xsl:if>
      </ul>
    </div>
  </xsl:template>



  <xsl:template name="make-footer">
    <footer id="footer">
      <div class="footer-updates row align-middle">
        <div class="medium-6 columns">
          <a href="#top" class="button transparent back-to-top icon">Back to Top</a>
        </div>
        <div class="medium-6 columns text-right"><span class="show-for-medium hide-for-small">Web
            page last updated on</span><span class="show-for-small hide-for-medium">Last
            updated</span> ADD DATE</div>
      </div>
      <div class="footer-nav">
        <div class="row">

          <div class="link-category column small-12 medium-6 large-2">
            <h5 class="no-toc">About NTP</h5>
            <ul class="vertical menu">
              <li>
                <a href="https://ntp.niehs.nih.gov/go/annualreport">Annual Report</a>
              </li>
              <li>
                <a href="https://ntp.niehs.nih.gov/ntp/pubs/currentdirections2014_508.pdf"
                  target="_blank" rel="noopener noreferrer">Current Directions</a>
              </li>
              <li>
                <a href="https://niehs.nih.gov/research/atniehs/dntp/index.cfm">NTP at NIEHS</a>
              </li>
              <li>
                <a href="https://ntp.niehs.nih.gov/about/org/index.html">Organization</a>
              </li>
            </ul>
          </div>
          <div class="link-category column small-12 medium-6 large-2">
            <h5 class="no-toc">News &amp; Media</h5>
            <ul class="vertical menu">
              <li>
                <a href="https://ntp.niehs.nih.gov/about/resources/faq/">FAQs &amp; Fact Sheets</a>
              </li>
              <li>
                <a href="https://ntp.niehs.nih.gov/go/frn">Federal Register Notices</a>
              </li>
              <li>
                <a href="https://ntp.niehs.nih.gov/go/news">News</a>
              </li>
              <li>
                <a href="https://ntp.niehs.nih.gov/go/ntpupdate">Update Newsletter</a>
              </li>
            </ul>
          </div>
          <div class="link-category column small-12 medium-6 large-2">
            <h5 class="no-toc">Get Involved</h5>
            <ul class="vertical menu">
              <li>
                <a href="https://ntp.niehs.nih.gov/go/calendar">Calendar &amp; Events</a>
              </li>
              <li>
                <a href="https://ntp.niehs.nih.gov/help/contactus/index.html">Contact NTP</a>
              </li>
              <li>
                <a href="https://ntp.niehs.nih.gov/go/input">Nominate to NTP</a>
              </li>
              <li>
                <a href="https://niehs.nih.gov/careers/research/postdoc-training/index.cfm"
                  >Postdoctoral Training</a>
              </li>
              <li>
                <a href="https://tools.niehs.nih.gov/webforms/index.cfm/main/formViewer/form_id/361"
                  >Subscribe to News Updates</a>
              </li>
            </ul>
          </div>
          <div class="link-category column small-12 medium-6 large-2">
            <h5 class="no-toc">Support</h5>
            <ul class="vertical menu">
              <li>
                <a
                  href="https://tools.niehs.nih.gov/webforms/index.cfm/main/formViewer/form_id/521?EMAILB=WEBMASTER"
                  >Contact Web Support</a>
              </li>
              <li>
                <a href="https://ntp.niehs.nih.gov/help">Help</a>
              </li>
              <li>
                <a href="https://ntp.niehs.nih.gov/help/site/index.html">Site Map</a>
              </li>
              <li>
                <a href="https://ntp.niehs.nih.gov/help/directions/index.html">Visiting NTP</a>
              </li>
            </ul>
          </div>

          <div class="footer-logos column small-12 medium-6 large-4">
            <a href="https://www.usa.gov" target="_blank" rel="noopener noreferrer">
              <img src="https://ntp.niehs.nih.gov/images/template/logo_usagov_dark.png"
                alt="USA.gov is the U.S. government's official web portal to all federal, state, and local government web resources and services"
                title="USA.gov: Government Made Easy" class="show-for-medium"/>
              <img src="https://ntp.niehs.nih.gov/images/template/logo_usagov.png"
                alt="USA.gov is the U.S. government's official web portal to all federal, state, and local government web resources and services"
                title="USA.gov: Government Made Easy" class="show-for-small-only"/>
            </a>
            <a href="https://www.hhs.gov" target="_blank" rel="noopener noreferrer">
              <img src="https://ntp.niehs.nih.gov/images/template/logo_hhs_dark.png"
                alt="U.S. Department of Health and Human Services"
                title="U.S. Department of Health and Human Services (HHS)" class="show-for-medium"/>
              <img src="https://ntp.niehs.nih.gov/images/template/logo_hhs.png"
                alt="U.S. Department of Health and Human Services"
                title="U.S. Department of Health and Human Services (HHS)"
                class="show-for-small-only"/>
            </a>
            <ul>
              <li>
                <a href="https://www.niehs.nih.gov/about/foia/index.cfm" target="_blank"
                  rel="noopener noreferrer">Freedom of Information Act</a>
              </li>
              <li>
                <a href="https://oig.hhs.gov" target="_blank" rel="noopener noreferrer">Inspector
                  General</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="footer-links">
        <div class="row medium-unstack">
          <div id="footer-ntp" class="column medium-8 small-12">NTP is located at the <a
              href="https://www.niehs.nih.gov" target="_blank" rel="noopener noreferrer"
              class="external">National Institute of Environmental Health Sciences</a>, part of the
              <a href="https://www.nih.gov" target="_blank" rel="noopener noreferrer"
              class="external">National Institutes of Health</a></div>
        </div>
      </div>
    </footer>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.5.3/js/foundation.min.js"/>
    <xsl:comment>OVERRIDE JS - START</xsl:comment>
    <script src="js/override.js"/>
    <xsl:comment>ARTICLE JS - END</xsl:comment>
  </xsl:template>
  


  <!-- ============================================================= -->
  <!--  TOP LEVEL                                                    -->
  <!-- ============================================================= -->

  <!--
      content model for article:
         (front,body?,back?,floats-group?,(sub-article*|response*))
      tabs full-width
      content model for sub-article:
         ((front|front-stub),body?,back?,floats-group?,
          (sub-article*|response*))
      
      content model for response:
         ((front|front-stub),body?,back?,floats-group?) -->

  <xsl:template match="article">
    <xsl:call-template name="make-article"/>
  </xsl:template>

  <xsl:template match="sub-article | response">
    <hr class="part-rule"/>
    <xsl:call-template name="make-article"/>
  </xsl:template>


  <!-- ============================================================= -->
  <!--  "make-article" for the document architecture                 -->
  <!-- ============================================================= -->


  <!-- Skip nodes to avoid duplicate content -->
  <xsl:template
    match="/book/collection-meta | /book/book-meta/book-id | /book/book-meta/book-title-group | /book/book-meta/contrib-group | /book/book-meta/pub-date | /book/book-meta/publisher"/>

  <xsl:template name="make-article">


    <!-- Generates a series of (flattened) divs for contents of any
	       article, sub-article or response -->

    <!-- variable to be used in div id's to keep them unique -->
    <xsl:variable name="this-article">
      <xsl:apply-templates select="." mode="id"/>
    </xsl:variable>

    <div id="{$this-article}-front" class="front">
      <xsl:apply-templates select="front | front-stub"/>
    </div>

    <!-- body -->
    <xsl:for-each select="body">
      <div id="{$this-article}-body" class="body">
        <xsl:apply-templates/>
      </div>
    </xsl:for-each>

    <xsl:if test="back | $loose-footnotes">
      <!-- $loose-footnotes is defined below as any footnotes outside
           front matter or fn-group -->
      <div id="{$this-article}-back" class="back">
        <xsl:call-template name="make-back"/>
      </div>
    </xsl:if>

    <xsl:for-each select="floats-group | floats-wrap">
      <!-- floats-wrap is from 2.3 -->
      <div id="{$this-article}-floats" class="back">
        <xsl:call-template name="main-title">
          <xsl:with-param name="contents">
            <span class="generated">Floating objects</span>
          </xsl:with-param>
        </xsl:call-template>
        <xsl:apply-templates/>
      </div>
    </xsl:for-each>


    <!-- sub-article or response (recursively calls
		     this template) -->
    <xsl:apply-templates select="sub-article | response"/>

  </xsl:template>

  <xsl:template match="front | front-stub">
    <!-- First Table: journal and article metadata -->
    <div class="metadata two-column table">
      <div class="row">
        <!-- Cell 1: journal information -->
        <xsl:for-each select="journal-meta">
          <!-- (journal-id+, journal-title-group*, (contrib-group | aff | aff-alternatives)*,
                issn+, issn-l?, isbn*, publisher?, notes*, self-uri*)         -->
          <div class="cell">
            <h4 class="generated">
              <xsl:text>Journal Information</xsl:text>
            </h4>
            <div class="metadata-group">
              <xsl:apply-templates select="journal-id | journal-title-group" mode="metadata"/>
              <!-- the following may appear in 2.3 -->
              <xsl:apply-templates mode="metadata"
                select="journal-title | journal-subtitle | trans-title | trans-subtitle | abbrev-journal-title"/>
              <!-- contrib-group, aff, aff-alternatives, author-notes -->
              <xsl:apply-templates mode="metadata" select="contrib-group"/>
              <xsl:if test="aff | aff-alternatives | author-notes">
                <div class="metadata-group">
                  <xsl:apply-templates mode="metadata"
                    select="aff | aff-alternatives | author-notes"/>
                </div>
              </xsl:if>
              <xsl:apply-templates select="issn | issn-l | isbn | publisher | notes | self-uri"
                mode="metadata"/>
            </div>
          </div>
        </xsl:for-each>

        <!-- Cell 2: Article information -->
        <xsl:for-each select="article-meta | self::front-stub">
          <!-- content model:
				    (article-id*, article-categories?, title-group,
				     (contrib-group | aff)*, 
             author-notes?, pub-date+, volume?, volume-id*,
             volume-series?, issue?, issue-id*, issue-title*,
             issue-sponsor*, issue-part?, isbn*, supplement?, 
             ((fpage, lpage?, page-range?) | elocation-id)?, 
             (email | ext-link | uri | product | 
              supplementary-material)*, 
             history?, permissions?, self-uri*, related-article*, 
             abstract*, trans-abstract*, 
             kwd-group*, funding-group*, conference*, counts?,
             custom-meta-group?)
            
            These are handled as follows:

            In the "Article Information" header cell:
              article-id
              pub-date
              volume
              volume-id
              volume-series
              issue
              issue-id
              issue-title
              issue-sponsor
              issue-part
              isbn
              supplement
              fpage
              lpage
              page-range
              elocation-id
              email
              ext-link
              uri
              product
              history
              permissions
              self-uri
              related-article
              funding-group
              conference

            In the "Article title" cell:
              title-group
              contrib-group
              aff
              author-notes
              abstract
              trans-abstract

            In the metadata footer
              article-categories
              supplementary-material
              kwd-group
              counts
              custom-meta-group

				  -->

          <div class="cell">
            <h4 class="generated">
              <xsl:text>Article Information</xsl:text>
            </h4>
            <div class="metadata-group">

              <xsl:apply-templates mode="metadata" select="email | ext-link | uri | self-uri"/>

              <xsl:apply-templates mode="metadata" select="product"/>

              <!-- only in 2.3 -->
              <xsl:apply-templates mode="metadata"
                select="
                  copyright-statement |
                  copyright-year | license"/>

              <xsl:apply-templates mode="metadata" select="permissions"/>

              <xsl:apply-templates mode="metadata" select="history/date"/>

              <xsl:apply-templates mode="metadata" select="pub-date"/>

              <xsl:call-template name="volume-info">
                <!-- handles volume?, volume-id*, volume-series? -->
              </xsl:call-template>

              <xsl:call-template name="issue-info">
                <!-- handles issue?, issue-id*, issue-title*,
                     issue-sponsor*, issue-part? -->
              </xsl:call-template>

              <xsl:call-template name="page-info">
                <!-- handles (fpage, lpage?, page-range?) -->
              </xsl:call-template>

              <xsl:apply-templates mode="metadata" select="elocation-id"/>

              <xsl:apply-templates mode="metadata" select="isbn"/>

              <xsl:apply-templates mode="metadata"
                select="supplement | related-article | conference"/>

              <xsl:apply-templates mode="metadata" select="article-id"/>

              <!-- only in 2.3 -->
              <xsl:apply-templates mode="metadata"
                select="
                  contract-num | contract-sponsor |
                  grant-num | grant-sponsor"/>

              <xsl:apply-templates mode="metadata" select="funding-group/*">
                <!-- includes (award-group*, funding-statement*,
                     open-access?) -->
              </xsl:apply-templates>
            </div>
          </div>
        </xsl:for-each>
      </div>
    </div>

    <hr class="part-rule"/>

    <!-- change context to front/article-meta (again) -->
    <xsl:for-each select="article-meta | self::front-stub">
      <div class="metadata centered">
        <xsl:apply-templates mode="metadata" select="title-group"/>
      </div>
      <!-- contrib-group, aff, aff-alternatives, author-notes -->
      <xsl:apply-templates mode="metadata" select="contrib-group"/>
      <!-- back in article-meta or front-stub context -->
      <xsl:if test="aff | aff-alternatives | author-notes">
        <div class="metadata two-column table">
          <div class="row">
            <div class="cell empty"/>
            <div class="cell">
              <div class="metadata-group">
                <xsl:apply-templates mode="metadata" select="aff | aff-alternatives | author-notes"
                />
              </div>
            </div>
          </div>
        </div>
      </xsl:if>

      <!-- abstract(s) -->
      <xsl:if test="abstract | trans-abstract">
        <!-- rule separates title+authors from abstract(s) -->
        <hr class="section-rule"/>

        <xsl:for-each select="abstract | trans-abstract">
          <!-- title in left column, content (paras, secs) in right -->
          <div class="metadata two-column table">
            <div class="row">
              <div class="cell" style="text-align: right">
                <h4 class="callout-title">
                  <xsl:apply-templates select="title/node()"/>
                  <xsl:if test="not(normalize-space(string(title)))">
                    <span class="generated">
                      <xsl:if test="self::trans-abstract">Translated </xsl:if>
                      <xsl:text>Abstract</xsl:text>
                    </span>
                  </xsl:if>
                </h4>
              </div>
              <div class="cell">
                <xsl:apply-templates select="*[not(self::title)]"/>
              </div>
            </div>
          </div>
        </xsl:for-each>
        <!-- end of abstract or trans-abstract -->
      </xsl:if>
      <!-- end of dealing with abstracts -->
    </xsl:for-each>
    <xsl:for-each select="notes">
      <div class="metadata">
        <xsl:apply-templates mode="metadata" select="."/>
      </div>
    </xsl:for-each>
    <hr class="part-rule"/>

    <!-- end of big front-matter pull -->
  </xsl:template>


  <xsl:template name="footer-metadata">
    <!-- handles: article-categories, kwd-group, counts, 
           supplementary-material, custom-meta-group
         Plus also generates a sheet of processing warnings
         -->
    <xsl:for-each select="front/article-meta | front-stub">
      <xsl:if
        test="
          article-categories | kwd-group | counts |
          supplementary-material | custom-meta-group |
          custom-meta-wrap">
        <!-- custom-meta-wrap is from NLM 2.3 -->
        <hr class="part-rule"/>
        <div class="metadata">
          <h4 class="generated">
            <xsl:text>Article Information (continued)</xsl:text>
          </h4>
          <div class="metadata-group">
            <xsl:apply-templates mode="metadata" select="supplementary-material"/>

            <xsl:apply-templates mode="metadata" select="article-categories | kwd-group | counts"/>

            <xsl:apply-templates mode="metadata" select="custom-meta-group | custom-meta-wrap"/>
          </div>
        </div>
      </xsl:if>
    </xsl:for-each>

    <xsl:variable name="process-warnings">
      <xsl:call-template name="process-warnings"/>
    </xsl:variable>

    <xsl:if test="normalize-space(string($process-warnings))">
      <hr class="part-rule"/>
      <div class="metadata one-column table">
        <!--<div class="row">
          <div class="cell spanning">
            
          </div>
        </div>-->
        <div class="row">
          <div class="cell spanning">
            <h4 class="generated">
              <xsl:text>Process warnings</xsl:text>
            </h4>
            <p>Warnings reported by the processor due to problematic markup follow:</p>
            <div class="metadata-group">
              <xsl:copy-of select="$process-warnings"/>
            </div>
          </div>
        </div>
      </div>
    </xsl:if>
  </xsl:template>


  <!-- ============================================================= -->
  <!--  METADATA PROCESSING                                          -->
  <!-- ============================================================= -->

  <!--  Includes mode "metadata" for front matter, along with 
      "metadata-inline" for metadata elements collapsed into 
      inline sequences, plus associated named templates            -->

  <!-- WAS journal-meta content:
       journal-id+, journal-title-group*, issn+, isbn*, publisher?,
       notes? -->
  <!-- (journal-id+, journal-title-group*, (contrib-group | aff | aff-alternatives)*,
       issn+, issn-l?, isbn*, publisher?, notes*, self-uri*) -->
  <xsl:template match="journal-id" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">
        <xsl:text>Journal ID</xsl:text>
        <xsl:for-each select="@journal-id-type">
          <xsl:text> (</xsl:text>
          <span class="data">
            <xsl:value-of select="."/>
          </span>
          <xsl:text>)</xsl:text>
        </xsl:for-each>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="journal-title-group" mode="metadata">
    <xsl:apply-templates mode="metadata"/>
  </xsl:template>


  <xsl:template match="issn" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">
        <xsl:text>ISSN</xsl:text>
        <xsl:call-template name="append-pub-type"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="issn-l" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">
        <xsl:text>ISSN-L</xsl:text>
        <xsl:call-template name="append-pub-type"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="isbn" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">
        <xsl:text>ISBN</xsl:text>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="publisher" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Publisher</xsl:with-param>
      <xsl:with-param name="contents">
        <xsl:apply-templates select="publisher-name" mode="metadata-inline"/>
        <xsl:apply-templates select="publisher-loc" mode="metadata-inline"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="publisher-name" mode="metadata-inline">
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="publisher-loc" mode="metadata-inline">
    <span class="generated"> (</span>
    <xsl:apply-templates/>
    <span class="generated">)</span>
  </xsl:template>


  <xsl:template match="notes" mode="metadata">
    <xsl:call-template name="metadata-area">
      <xsl:with-param name="label">Notes</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <!-- journal-title-group content:
       (journal-title*, journal-subtitle*, trans-title-group*,
       abbrev-journal-title*) -->

  <xsl:template match="journal-title" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Title</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="journal-subtitle" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Subtitle</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="trans-title-group" mode="metadata">
    <xsl:apply-templates mode="metadata"/>
  </xsl:template>


  <xsl:template match="abbrev-journal-title" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Abbreviated Title</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <!-- trans-title-group content: (trans-title, trans-subtitle*) -->

  <xsl:template match="trans-title" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">
        <xsl:text>Translated Title</xsl:text>
        <xsl:for-each select="(../@xml:lang | @xml:lang)[last()]">
          <xsl:text> (</xsl:text>
          <span class="data">
            <xsl:value-of select="."/>
          </span>
          <xsl:text>)</xsl:text>
        </xsl:for-each>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="trans-subtitle" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">
        <xsl:text>Translated Subtitle</xsl:text>
        <xsl:for-each select="(../@xml:lang | @xml:lang)[last()]">
          <xsl:text> (</xsl:text>
          <span class="data">
            <xsl:value-of select="."/>
          </span>
          <xsl:text>)</xsl:text>
        </xsl:for-each>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <!-- article-meta content:
    (article-id*, article-categories?, title-group,
     (contrib-group | aff)*, author-notes?, pub-date+, volume?,
     volume-id*, volume-series?, issue?, issue-id*, 
     issue-title*, issue-sponsor*, issue-part?, isbn*, 
     supplement?, ((fpage, lpage?, page-range?) | elocation-id)?, 
     (email | ext-link | uri | product | supplementary-material)*, 
     history?, permissions?, self-uri*, related-article*,
     abstract*, trans-abstract*, 
     kwd-group*, funding-group*, conference*, counts?, 
     custom-meta-group?) -->

  <!-- In order of appearance... -->

  <xsl:template match="ext-link" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">
        <xsl:text>External link</xsl:text>
        <xsl:for-each select="ext-link-type">
          <xsl:text> (</xsl:text>
          <span class="data">
            <xsl:value-of select="."/>
          </span>
          <xsl:text>)</xsl:text>
        </xsl:for-each>
      </xsl:with-param>
      <xsl:with-param name="contents">
        <xsl:apply-templates select="."/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="email" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Email</xsl:with-param>
      <xsl:with-param name="contents">
        <xsl:apply-templates select="."/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="uri" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">URI</xsl:with-param>
      <xsl:with-param name="contents">
        <xsl:apply-templates select="."/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="self-uri" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">
        <xsl:text>Self URI</xsl:text>
      </xsl:with-param>
      <xsl:with-param name="contents">
        <a href="{@xlink:href}">
          <xsl:choose>
            <xsl:when test="normalize-space(string(.))">
              <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="@xlink:href"/>
            </xsl:otherwise>
          </xsl:choose>
        </a>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="product" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">
        <xsl:text>Product Information</xsl:text>
      </xsl:with-param>
      <xsl:with-param name="contents">
        <xsl:choose>
          <xsl:when test="normalize-space(string(@xlink:href))">
            <a>
              <xsl:call-template name="assign-href"/>
              <xsl:apply-templates/>
            </a>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="permissions" mode="metadata">
    <xsl:apply-templates select="copyright-statement" mode="metadata"/>
    <xsl:if test="copyright-year | copyright-holder">
      <xsl:call-template name="metadata-labeled-entry">
        <xsl:with-param name="label">Copyright</xsl:with-param>
        <xsl:with-param name="contents">
          <xsl:for-each select="copyright-year | copyright-holder">
            <xsl:apply-templates/>
            <xsl:if test="not(position() = last())">, </xsl:if>
          </xsl:for-each>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:if>
    <xsl:apply-templates select="license" mode="metadata"/>
  </xsl:template>


  <xsl:template match="copyright-statement" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Copyright statement</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="copyright-year" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Copyright</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="license" mode="metadata">
    <xsl:call-template name="metadata-area">
      <xsl:with-param name="label">
        <xsl:text>License</xsl:text>
        <xsl:if test="@license-type | @xlink:href">
          <xsl:text> (</xsl:text>
          <span class="data">
            <xsl:value-of select="@license-type"/>
            <xsl:if test="@xlink:href">
              <xsl:if test="@license-type">, </xsl:if>
              <a>
                <xsl:call-template name="assign-href"/>
                <xsl:value-of select="@xlink:href"/>
              </a>
            </xsl:if>
          </span>
          <xsl:text>)</xsl:text>
        </xsl:if>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="history/date" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">
        <xsl:text>Date</xsl:text>
        <xsl:for-each select="@date-type">
          <xsl:choose>
            <xsl:when test=". = 'accepted'"> accepted</xsl:when>
            <xsl:when test=". = 'received'"> received</xsl:when>
            <xsl:when test=". = 'rev-request'"> revision requested</xsl:when>
            <xsl:when test=". = 'rev-recd'"> revision received</xsl:when>
          </xsl:choose>
        </xsl:for-each>
      </xsl:with-param>
      <xsl:with-param name="contents">
        <xsl:call-template name="format-date"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="pub-date" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">
        <xsl:text>Publication date</xsl:text>
        <xsl:call-template name="append-pub-type"/>
      </xsl:with-param>
      <xsl:with-param name="contents">
        <xsl:call-template name="format-date"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template name="volume-info">
    <!-- handles volume?, volume-id*, volume-series? -->
    <xsl:if test="volume | volume-id | volume-series">
      <xsl:choose>
        <xsl:when test="not(volume-id[2]) or not(volume)">
          <!-- if there are no multiple volume-id, or no volume, we make one line only -->
          <xsl:call-template name="metadata-labeled-entry">
            <xsl:with-param name="label">Volume</xsl:with-param>
            <xsl:with-param name="contents">
              <xsl:apply-templates select="volume | volume-series" mode="metadata-inline"/>
              <xsl:apply-templates select="volume-id" mode="metadata-inline"/>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="volume | volume-id | volume-series" mode="metadata"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>


  <xsl:template match="volume | issue" mode="metadata-inline">
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="volume-id | issue-id" mode="metadata-inline">
    <span class="generated">
      <xsl:text> (</xsl:text>
      <xsl:for-each select="@pub-id-type">
        <span class="data">
          <xsl:value-of select="."/>
        </span>
        <xsl:text> </xsl:text>
      </xsl:for-each>
      <xsl:text>ID: </xsl:text>
    </span>
    <xsl:apply-templates/>
    <span class="generated">)</span>
  </xsl:template>


  <xsl:template match="volume-series" mode="metadata-inline">
    <xsl:if test="preceding-sibling::volume">
      <span class="generated">,</span>
    </xsl:if>
    <xsl:text> </xsl:text>
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="volume" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Volume</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="volume-id" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">
        <xsl:text>Volume ID</xsl:text>
        <xsl:for-each select="@pub-id-type">
          <xsl:text> (</xsl:text>
          <span class="data">
            <xsl:value-of select="."/>
          </span>
          <xsl:text>)</xsl:text>
        </xsl:for-each>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="volume-series" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Series</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template name="issue-info">
    <!-- handles issue?, issue-id*, issue-title*, issue-sponsor*, issue-part?, supplement? -->
    <xsl:variable name="issue-info"
      select="
        issue | issue-id | issue-title |
        issue-sponsor | issue-part"/>
    <xsl:choose>
      <xsl:when
        test="$issue-info and not(issue-id[2] | issue-title[2] | issue-sponsor | issue-part)">
        <!-- if there are only one issue, issue-id and issue-title and nothing else, we make one line only -->
        <xsl:call-template name="metadata-labeled-entry">
          <xsl:with-param name="label">Issue</xsl:with-param>
          <xsl:with-param name="contents">
            <xsl:apply-templates select="issue | issue-title" mode="metadata-inline"/>
            <xsl:apply-templates select="issue-id" mode="metadata-inline"/>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="$issue-info" mode="metadata"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>


  <xsl:template match="issue-title" mode="metadata-inline">
    <span class="generated">
      <xsl:if test="preceding-sibling::issue">,</xsl:if>
    </span>
    <xsl:text> </xsl:text>
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="issue" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Issue</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="issue-id" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">
        <xsl:text>Issue ID</xsl:text>
        <xsl:for-each select="@pub-id-type">
          <xsl:text> (</xsl:text>
          <span class="data">
            <xsl:value-of select="."/>
          </span>
          <xsl:text>)</xsl:text>
        </xsl:for-each>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="issue-title" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Issue title</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="issue-sponsor" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Issue sponsor</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="issue-part" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Issue part</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template name="page-info">
    <!-- handles (fpage, lpage?, page-range?) -->
    <xsl:if test="fpage | lpage | page-range">
      <xsl:call-template name="metadata-labeled-entry">
        <xsl:with-param name="label">
          <xsl:text>Page</xsl:text>
          <xsl:if
            test="
              normalize-space(string(lpage[not(. = ../fpage)]))
              or normalize-space(string(page-range))">
            <!-- we have multiple pages if lpage exists and is not equal fpage,
               or if we have a page-range -->
            <xsl:text>s</xsl:text>
          </xsl:if>
        </xsl:with-param>
        <xsl:with-param name="contents">
          <xsl:value-of select="fpage"/>
          <xsl:if test="normalize-space(string(lpage[not(. = ../fpage)]))">
            <xsl:text>-</xsl:text>
            <xsl:value-of select="lpage"/>
          </xsl:if>
          <xsl:for-each select="page-range">
            <xsl:text> (pp. </xsl:text>
            <xsl:value-of select="."/>
            <xsl:text>)</xsl:text>
          </xsl:for-each>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>


  <xsl:template match="elocation-id" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Electronic Location Identifier</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <!-- isbn is already matched in mode 'metadata' above -->

  <xsl:template match="supplement" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Supplement Info</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="related-article | related-object" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">
        <xsl:text>Related </xsl:text>
        <xsl:choose>
          <xsl:when test="self::related-object">object</xsl:when>
          <xsl:otherwise>article</xsl:otherwise>
        </xsl:choose>
        <xsl:for-each select="@related-article-type | @object-type">
          <xsl:text> (</xsl:text>
          <span class="data">
            <xsl:value-of select="translate(., '-', ' ')"/>
          </span>
          <xsl:text>)</xsl:text>
        </xsl:for-each>
      </xsl:with-param>
      <xsl:with-param name="contents">
        <xsl:choose>
          <xsl:when test="normalize-space(string(@xlink:href))">
            <a>
              <xsl:call-template name="assign-href"/>
              <xsl:apply-templates/>
            </a>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="conference" mode="metadata">
    <!-- content model:
      (conf-date, 
       (conf-name | conf-acronym)+, 
       conf-num?, conf-loc?, conf-sponsor*, conf-theme?) -->
    <xsl:choose>
      <xsl:when
        test="
          not(conf-name[2] | conf-acronym[2] | conf-sponsor |
          conf-theme)">
        <!-- if there is no second name or acronym, and no sponsor
             or theme, we make one line only -->
        <xsl:call-template name="metadata-labeled-entry">
          <xsl:with-param name="label">Conference</xsl:with-param>
          <xsl:with-param name="contents">
            <xsl:apply-templates select="conf-acronym | conf-name" mode="metadata-inline"/>
            <xsl:apply-templates select="conf-num" mode="metadata-inline"/>
            <xsl:if test="conf-date | conf-loc">
              <span class="generated"> (</span>
              <xsl:for-each select="conf-date | conf-loc">
                <xsl:if test="position() = 2">, </xsl:if>
                <xsl:apply-templates select="." mode="metadata-inline"/>
              </xsl:for-each>
              <span class="generated">)</span>
            </xsl:if>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates mode="metadata"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>


  <xsl:template match="conf-date" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Conference date</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="conf-name" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Conference</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="conf-acronym" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Conference</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="conf-num" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Conference number</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="conf-loc" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Conference location</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="conf-sponsor" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Conference sponsor</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="conf-theme" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Conference theme</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="conf-name | conf-acronym" mode="metadata-inline">
    <!-- we only hit this template if there is at most one of each -->
    <xsl:variable name="following"
      select="preceding-sibling::conf-name | preceding-sibling::conf-acronym"/>
    <!-- if we come after the other, we go in parentheses -->
    <xsl:if test="$following">
      <span class="generated"> (</span>
    </xsl:if>
    <xsl:apply-templates/>
    <xsl:if test="$following">
      <span class="generated">)</span>
    </xsl:if>
  </xsl:template>


  <xsl:template match="conf-num" mode="metadata-inline">
    <xsl:text> </xsl:text>
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="conf-date | conf-loc" mode="metadata-inline">
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="article-id" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">
        <xsl:choose>
          <xsl:when test="@pub-id-type = 'art-access-id'">Accession ID</xsl:when>
          <xsl:when test="@pub-id-type = 'coden'">Coden</xsl:when>
          <xsl:when test="@pub-id-type = 'doi'">DOI</xsl:when>
          <xsl:when test="@pub-id-type = 'manuscript'">Manuscript ID</xsl:when>
          <xsl:when test="@pub-id-type = 'medline'">Medline ID</xsl:when>
          <xsl:when test="@pub-id-type = 'pii'">Publisher Item ID</xsl:when>
          <xsl:when test="@pub-id-type = 'pmid'">PubMed ID</xsl:when>
          <xsl:when test="@pub-id-type = 'publisher-id'">Publisher ID</xsl:when>
          <xsl:when test="@pub-id-type = 'sici'">Serial Item and Contribution ID</xsl:when>
          <xsl:when test="@pub-id-type = 'doaj'">DOAJ ID</xsl:when>
          <xsl:when test="@pub-id-type = 'arXiv'">arXiv.org ID</xsl:when>
          <xsl:otherwise>
            <xsl:text>Article Id</xsl:text>
            <xsl:for-each select="@pub-id-type">
              <xsl:text> (</xsl:text>
              <span class="data">
                <xsl:value-of select="."/>
              </span>
              <xsl:text>)</xsl:text>
            </xsl:for-each>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="contract-num" mode="metadata">
    <!-- only in 2.3 -->
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Contract</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="contract-sponsor" mode="metadata">
    <!-- only in 2.3 -->
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Contract Sponsor</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="grant-num" mode="metadata">
    <!-- only in 2.3 -->
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Grant Number</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="grant-sponsor" mode="metadata">
    <!-- only in 2.3 -->
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Grant Sponsor</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="award-group" mode="metadata">
    <!-- includes (funding-source*, award-id*, principal-award-recipient*, principal-investigator*) -->
    <xsl:apply-templates mode="metadata"/>
  </xsl:template>


  <xsl:template match="funding-source" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Funded by</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="award-id" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Award ID</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="principal-award-recipient" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Award Recipient</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="principal-investigator" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Principal Investigator</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="funding-statement" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Funding</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="open-access" mode="metadata">
    <xsl:call-template name="metadata-area">
      <xsl:with-param name="label">Open Access</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="title-group" mode="metadata">
    <!-- content model:
    article-title, subtitle*, trans-title-group*, alt-title*, fn-group? -->
    <!-- trans-title and trans-subtitle included for 2.3 -->
    <xsl:apply-templates
      select="
        article-title | subtitle | trans-title-group |
        trans-title | trans-subtitle"
      mode="metadata"/>
    <xsl:if test="alt-title | fn-group">
      <div class="document-title-notes metadata-group">
        <xsl:apply-templates select="alt-title | fn-group" mode="metadata"/>
      </div>
    </xsl:if>
  </xsl:template>


  <xsl:template match="title-group/article-title" mode="metadata">
    <h1 class="document-title">
      <xsl:apply-templates/>
      <xsl:if test="../subtitle">:</xsl:if>
    </h1>
  </xsl:template>


  <xsl:template match="title-group/subtitle | trans-title-group/subtitle" mode="metadata">
    <h2 class="document-title">
      <xsl:apply-templates/>
    </h2>
  </xsl:template>


  <xsl:template match="title-group/trans-title-group" mode="metadata">
    <!-- content model: (trans-title, trans-subtitle*) -->
    <xsl:apply-templates mode="metadata"/>
  </xsl:template>



  <xsl:template match="title-group/alt-title" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">
        <xsl:text>Alternative title</xsl:text>
        <xsl:for-each select="@alt-title-type">
          <xsl:text> (</xsl:text>
          <span class="data">
            <xsl:value-of select="."/>
          </span>
          <xsl:text>)</xsl:text>
        </xsl:for-each>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="title-group/fn-group" mode="metadata">
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template mode="metadata" match="journal-meta/contrib-group">
    <xsl:for-each select="contrib">
      <xsl:variable name="contrib-identification">
        <xsl:call-template name="contrib-identify"/>
      </xsl:variable>
      <!-- placing the div only if it has content -->
      <!-- the extra call to string() makes it type-safe in a type-aware
           XSLT 2.0 engine -->
      <xsl:if test="normalize-space(string($contrib-identification))">
        <xsl:copy-of select="$contrib-identification"/>
      </xsl:if>
      <xsl:variable name="contrib-info">
        <xsl:call-template name="contrib-info"/>
      </xsl:variable>
      <!-- placing the div only if it has content -->
      <xsl:if test="normalize-space(string($contrib-info))">
        <xsl:copy-of select="$contrib-info"/>
      </xsl:if>
    </xsl:for-each>

    <xsl:if test="*[not(self::contrib | self::xref)]">

      <xsl:apply-templates mode="metadata" select="*[not(self::contrib | self::xref)]"/>

    </xsl:if>
  </xsl:template>

  <xsl:template mode="metadata" match="article-meta/contrib-group">
    <!-- content model of contrib-group:
        (contrib+, 
        (address | aff | author-comment | bio | email |
        ext-link | on-behalf-of | role | uri | xref)*) -->
    <!-- each contrib makes a row: name at left, details at right -->
    <xsl:for-each select="contrib">
      <!--  content model of contrib:
          ((contrib-id)*,
           (anonymous | collab | collab-alternatives | name | name-alternatives)*,
           (degrees)*,
           (address | aff | aff-alternatives | author-comment | bio | email |
            ext-link | on-behalf-of | role | uri | xref)*)       -->
      <div class="metadata two-column table">
        <div class="row">
          <div class="cell" style="text-align: right">
            <xsl:call-template name="contrib-identify">
              <!-- handles (contrib-id)*,
                (anonymous | collab | collab-alternatives |
                 name | name-alternatives | degrees | xref) -->
            </xsl:call-template>
          </div>
          <div class="cell">
            <xsl:call-template name="contrib-info">
              <!-- handles
                   (address | aff | author-comment | bio | email |
                    ext-link | on-behalf-of | role | uri) -->
            </xsl:call-template>
          </div>
        </div>
      </div>
    </xsl:for-each>
    <!-- end of contrib -->
    <xsl:variable name="misc-contrib-data" select="*[not(self::contrib | self::xref)]"/>
    <xsl:if test="$misc-contrib-data">
      <div class="metadata two-column table">
        <div class="row">
          <div class="cell">&#160;</div>
          <div class="cell">
            <div class="metadata-group">
              <xsl:apply-templates mode="metadata" select="$misc-contrib-data"/>
            </div>
          </div>
        </div>
      </div>
    </xsl:if>
  </xsl:template>


  <xsl:template name="contrib-identify">
    <!-- Placed in a left-hand pane  -->
    <!--handles
    (anonymous | collab | collab-alternatives |
    name | name-alternatives | degrees | xref)
    and @equal-contrib -->
    <div class="metadata-group">
      <xsl:for-each
        select="
          anonymous |
          collab | collab-alternatives/* | name | name-alternatives/*">
        <xsl:call-template name="metadata-entry">
          <xsl:with-param name="contents">
            <xsl:if test="position() = 1">
              <!-- a named anchor for the contrib goes with its
              first member -->
              <xsl:call-template name="named-anchor"/>
              <!-- so do any contrib-ids -->
              <xsl:apply-templates mode="metadata-inline" select="../contrib-id"/>
            </xsl:if>
            <xsl:apply-templates select="." mode="metadata-inline"/>
            <xsl:if test="position() = last()">
              <xsl:apply-templates mode="metadata-inline" select="degrees | xref"/>
              <!-- xrefs in the parent contrib-group go with the last member
              of *each* contrib in the group -->
              <xsl:apply-templates mode="metadata-inline" select="following-sibling::xref"/>
            </xsl:if>

          </xsl:with-param>
        </xsl:call-template>
      </xsl:for-each>
      <xsl:if test="@equal-contrib = 'yes'">
        <xsl:call-template name="metadata-entry">
          <xsl:with-param name="contents">
            <span class="generated">(Equal contributor)</span>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:if>
    </div>
  </xsl:template>


  <xsl:template match="anonymous" mode="metadata-inline">
    <xsl:text>Anonymous</xsl:text>
  </xsl:template>


  <xsl:template match="
      collab |
      collab-alternatives/*" mode="metadata-inline">
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="
      contrib/name |
      contrib/name-alternatives/*"
    mode="metadata-inline">
    <xsl:apply-templates select="."/>
  </xsl:template>


  <xsl:template match="degrees" mode="metadata-inline">
    <xsl:text>, </xsl:text>
    <xsl:apply-templates/>
  </xsl:template>




  <xsl:template name="contrib-info">
    <!-- Placed in a right-hand pane -->
    <div class="metadata-group">
      <xsl:apply-templates mode="metadata"
        select="
          address | aff | author-comment | bio | email |
          ext-link | on-behalf-of | role | uri"
      />
    </div>
  </xsl:template>


  <xsl:template mode="metadata" match="address[not(addr-line) or not(*[2])]">
    <!-- when we have no addr-line or a single child, we generate
         a single unlabelled line -->
    <xsl:call-template name="metadata-entry">
      <xsl:with-param name="contents">
        <xsl:call-template name="address-line"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="address" mode="metadata">
    <!-- when we have an addr-line we generate an unlabelled block -->
    <xsl:call-template name="metadata-area">
      <xsl:with-param name="contents">
        <xsl:apply-templates mode="metadata"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template mode="metadata" priority="2" match="address/*">
    <!-- being sure to override other templates for these
         element types -->
    <xsl:call-template name="metadata-entry"/>
  </xsl:template>


  <xsl:template match="aff" mode="metadata">
    <xsl:call-template name="metadata-entry">
      <xsl:with-param name="contents">
        <xsl:apply-templates/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="author-comment" mode="metadata">
    <xsl:call-template name="metadata-area">
      <xsl:with-param name="label">Comment</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="bio" mode="metadata">
    <xsl:call-template name="metadata-area">
      <xsl:with-param name="label">Bio</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="on-behalf-of" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">On behalf of</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="role" mode="metadata">
    <xsl:call-template name="metadata-entry"/>
  </xsl:template>


  <xsl:template match="author-notes" mode="metadata">
    <xsl:call-template name="metadata-area">
      <xsl:with-param name="label">Author notes</xsl:with-param>
      <xsl:with-param name="contents">
        <xsl:call-template name="named-anchor"/>
        <xsl:apply-templates mode="metadata"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="author-notes/corresp" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">
        <xsl:call-template name="named-anchor"/>
        <xsl:text>Correspondence to</xsl:text>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="author-notes/fn | author-notes/p" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">
        <xsl:apply-templates select="@fn-type"/>
      </xsl:with-param>
      <xsl:with-param name="contents">
        <xsl:call-template name="named-anchor"/>
        <xsl:apply-templates/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="supplementary-material" mode="metadata">
    <xsl:call-template name="metadata-area">
      <xsl:with-param name="label">Supplementary material</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="article-categories" mode="metadata">
    <xsl:apply-templates mode="metadata"/>
  </xsl:template>


  <xsl:template match="article-categories/subj-group" mode="metadata">
    <xsl:call-template name="metadata-area">
      <xsl:with-param name="label">Categories</xsl:with-param>
      <xsl:with-param name="contents">
        <xsl:apply-templates mode="metadata"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="subj-group" mode="metadata">
    <xsl:apply-templates mode="metadata"/>
  </xsl:template>


  <xsl:template match="subj-group/subj-group" mode="metadata">
    <div class="metadata-area">
      <xsl:apply-templates mode="metadata"/>
    </div>
  </xsl:template>


  <xsl:template match="subj-group/subject" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Subject</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="series-title" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Series title</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="series-text" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Series description</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="kwd-group" mode="metadata">
    <xsl:call-template name="metadata-area">
      <xsl:with-param name="label">
        <xsl:apply-templates select="title | label" mode="metadata-inline"/>
        <xsl:if test="not(title | label)">Keywords</xsl:if>
      </xsl:with-param>
      <xsl:with-param name="contents">
        <xsl:apply-templates mode="metadata"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="title" mode="metadata">
    <xsl:apply-templates select="."/>
  </xsl:template>


  <xsl:template match="kwd" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">Keyword</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="nested-kwd" mode="metadata">
    <ul class="nested-kwd">
      <xsl:apply-templates mode="metadata"/>
    </ul>
  </xsl:template>

  <xsl:template match="nested-kwd/kwd" mode="metadata">
    <li class="kwd">
      <xsl:apply-templates/>
    </li>
  </xsl:template>


  <xsl:template match="compound-kwd" mode="metadata">
    <xsl:call-template name="metadata-area">
      <xsl:with-param name="label">Compound keyword</xsl:with-param>
      <xsl:with-param name="contents">
        <xsl:apply-templates mode="metadata"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="compound-kwd-part" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">
        <xsl:text>Keyword part</xsl:text>
        <xsl:for-each select="@content-type">
          <xsl:text> (</xsl:text>
          <span class="data">
            <xsl:value-of select="."/>
          </span>
          <xsl:text>)</xsl:text>
        </xsl:for-each>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="counts" mode="metadata">
    <!-- fig-count?, table-count?, equation-count?, ref-count?,
         page-count?, word-count? -->
    <xsl:call-template name="metadata-area">
      <xsl:with-param name="label">Counts</xsl:with-param>
      <xsl:with-param name="contents">
        <xsl:apply-templates mode="metadata"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template mode="metadata"
    match="
      count | fig-count | table-count | equation-count |
      ref-count | page-count | word-count">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">
        <xsl:apply-templates select="." mode="metadata-label"/>
      </xsl:with-param>
      <xsl:with-param name="contents">
        <xsl:value-of select="@count"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="count" mode="metadata-label">
    <xsl:text>Count</xsl:text>
    <xsl:for-each select="@count-type">
      <xsl:text> (</xsl:text>
      <span class="data">
        <xsl:value-of select="."/>
      </span>
      <xsl:text>)</xsl:text>
    </xsl:for-each>
  </xsl:template>


  <xsl:template match="fig-count" mode="metadata-label">Figures</xsl:template>


  <xsl:template match="table-count" mode="metadata-label">Tables</xsl:template>


  <xsl:template match="equation-count" mode="metadata-label">Equations</xsl:template>


  <xsl:template match="ref-count" mode="metadata-label">References</xsl:template>


  <xsl:template match="page-count" mode="metadata-label">Pages</xsl:template>


  <xsl:template match="word-count" mode="metadata-label">Words</xsl:template>


  <xsl:template mode="metadata" match="custom-meta-group | custom-meta-wrap">
    <xsl:call-template name="metadata-area">
      <xsl:with-param name="label">Custom metadata</xsl:with-param>
      <xsl:with-param name="contents">
        <xsl:apply-templates mode="metadata"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="custom-meta" mode="metadata">
    <xsl:call-template name="metadata-labeled-entry">
      <xsl:with-param name="label">
        <span class="data">
          <xsl:apply-templates select="meta-name" mode="metadata-inline"/>
        </span>
      </xsl:with-param>
      <xsl:with-param name="contents">
        <xsl:apply-templates select="meta-value" mode="metadata-inline"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="meta-name | meta-value" mode="metadata-inline">
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="sec">
    <div class="section">
      <xsl:apply-templates select="title"/>
      <xsl:apply-templates select="sec-meta"/>
      <xsl:apply-templates mode="drop-title"/>
    </div>
  </xsl:template>


  <xsl:template match="*" mode="drop-title">
    <xsl:apply-templates select="."/>
  </xsl:template>


  <xsl:template match="title | sec-meta" mode="drop-title"/>


  <xsl:template match="app">
    <div class="section app">
      <xsl:call-template name="named-anchor"/>
      <xsl:apply-templates select="." mode="label"/>
      <xsl:apply-templates/>
    </div>
  </xsl:template>


  <!--<xsl:template match="ref-list" name="ref-list">
    <div class="section ref-list">
      <xsl:call-template name="named-anchor"/>
      <xsl:apply-templates select="." mode="label"/>
      <xsl:apply-templates select="*[not(self::ref | self::ref-list)]"/>
      <xsl:if test="ref">
        <div class="ref-list table">
          <xsl:apply-templates select="ref"/>
        </div>
      </xsl:if>
      <xsl:apply-templates select="ref-list"/>
    </div>
  </xsl:template>-->


  <xsl:template match="sec-meta">
    <div class="section-metadata">
      <!-- includes contrib-group | permissions | kwd-group -->
      <xsl:apply-templates/>
    </div>
  </xsl:template>


  <xsl:template match="sec-meta/contrib-group">
    <xsl:apply-templates mode="metadata"/>
  </xsl:template>


  <xsl:template match="sec-meta/kwd-group">
    <!-- matches only if contrib-group has only contrib children -->
    <xsl:apply-templates select="." mode="metadata"/>
  </xsl:template>


  <!-- ============================================================= -->
  <!--  Titles                                                       -->
  <!-- ============================================================= -->

  <xsl:template name="main-title"
    match="
      body/*/title |
      back/title | back[not(title)]/*/title">
    <xsl:param name="contents">
      <xsl:apply-templates/>
    </xsl:param>
    <xsl:if test="normalize-space(string($contents))">
      <!-- coding defensively since empty titles make glitchy HTML -->
      <h3>
        <xsl:copy-of select="$contents"/>
      </h3>
    </xsl:if>
  </xsl:template>

  <xsl:template name="section-title"
    match="
      abstract/*/title | sec[@disp-level = '2']/title |
      back[title]/*/title | back[not(title)]/*/*/title | /book/front-matter/foreword/book-part-meta/title-group/title | /book/front-matter/front-matter-part/book-part-meta/title-group/title"> 
    <xsl:param name="contents">
      <xsl:apply-templates/>
    </xsl:param>
    <xsl:if test="normalize-space(string($contents))">
      <div class="anchor-top"><a href="#top" class="button transparent back-to-top icon">Back to Top</a></div>
      <div class="section-heading"><h3 id="{$contents}">
        <xsl:copy-of select="$contents"/>
      </h3></div>
    </xsl:if>
  </xsl:template>


  <xsl:template name="subsection-title"
    match="
      abstract/*/*/title | sec[@disp-level = '3']/title | body/*/*/*/titlebody/*/*/title |
      back[title]/*/*/title | back[not(title)]/*/*/*/title | /book/front-matter/front-matter-part/named-book-part-body/sec/title">
    <xsl:param name="contents">
      <xsl:apply-templates/>
    </xsl:param>
    <xsl:if test="normalize-space(string($contents))">
      <!-- coding defensively since empty titles make glitchy HTML -->
      <h4 class="subsection-title">
        <xsl:copy-of select="$contents"/>
      </h4>
    </xsl:if>
  </xsl:template>


  <xsl:template name="block-title" priority="2"
    match="
      list/title | def-list/title | boxed-text/title |
      verse-group/title | glossary/title | gloss-group/title | kwd-group/title">
    <xsl:param name="contents">
      <xsl:apply-templates/>
    </xsl:param>
    <xsl:if test="normalize-space(string($contents))">
      <!-- coding defensively since empty titles make glitchy HTML -->
      <h4 class="block-title">
        <xsl:copy-of select="$contents"/>
      </h4>
    </xsl:if>
  </xsl:template>

  <!-- match for tab content divs -->

  <xsl:template match="/book/front-matter">
    <div class="tabs-panel" id="about">
      <div class="row">
        <div class="columns">
          <xsl:choose>
            <xsl:when test="front-matter/foreword/book-part-meta/title-group/title | front-matter-part/book-part-meta/title-group/title">
              <xsl:attribute name="class">large-9 medium-12 small-12</xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="class">large-12 medium-12 small-12</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <h2>About this Report</h2>
          <xsl:apply-templates/>
        </div>
        <xsl:if test="foreword/book-part-meta/title-group/title | front-matter-part/book-part-meta/title-group/title">
          <div class="large-3 medium-12 small-12 columns rightrail">
            <ul class="accordion" data-accordion="" data-allow-all-closed="true" data-multi-expand="true">
              <li class="accordion-item is-active" data-accordion-item="" aria-expanded="true" aria-selected="true">
                <a href="#" class="accordion-title" aria-controls="wp9ekv-accordion" role="tab" id="on-this-page" aria-expanded="false" aria-selected="false">Navigate this Section</a>
                <div class="accordion-content" data-tab-content="" role="tabpanel" aria-labelledby="on-this-page" aria-hidden="false" id="wp9ekv-accordion" style="display: block;">
                  <ul>
                    <xsl:for-each select="foreword/book-part-meta/title-group/title">
                      <xsl:variable name="sections"><xsl:value-of select="."/></xsl:variable>
                      <li><a href="#{$sections}"><xsl:value-of select="$sections"/></a></li>
                    </xsl:for-each>
                    <xsl:for-each select="front-matter-part/book-part-meta/title-group/title">
                      <xsl:variable name="sections"><xsl:value-of select="."/></xsl:variable>
                      <li><a href="#{$sections}"><xsl:value-of select="$sections"/></a></li>
                    </xsl:for-each>
                  </ul>
                </div>
              </li>
              
            </ul>
          </div>
        </xsl:if>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="/book/book-meta/abstract">
    <xsl:variable name="title">
      <xsl:value-of select="title"/>
    </xsl:variable>
    <div class="tabs-panel is-active" id="{$title}">
      <div class="row">
        <div class="columns">
          <xsl:choose>
            <xsl:when test="body/sec[@disp-level = '2']/title">
              <xsl:attribute name="class">large-9 medium-12 small-12</xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="class">large-9 medium-12 small-12</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:apply-templates/>
        </div>
        
          <div class="large-3 medium-12 small-12 columns rightrail">
            <xsl:if test="body/sec[@disp-level = '2']/title">
            <ul class="accordion" data-accordion="" data-allow-all-closed="true" data-multi-expand="true">
              <li class="accordion-item is-active" data-accordion-item="" aria-expanded="true" aria-selected="true">
                <a href="#" class="accordion-title" aria-controls="wp9ekv-accordion" role="tab" id="on-this-page" aria-expanded="false" aria-selected="false">Navigate this Section</a>
                <div class="accordion-content" data-tab-content="" role="tabpanel" aria-labelledby="on-this-page" aria-hidden="false" id="wp9ekv-accordion" style="display: block;">
                  <ul>
                    <xsl:for-each select="body/sec[@disp-level = '2']/title">
                      <xsl:variable name="sections"><xsl:value-of select="."/></xsl:variable>
                      <li><a href="#{$sections}"><xsl:value-of select="$sections"/></a></li>
                    </xsl:for-each>
                  </ul>
                </div>
              </li>
              
            </ul>
            </xsl:if>
            <xsl:call-template name="right-col-figures-tables"/>
          </div>
     
        
      </div>
    </div>
  </xsl:template>
  
  <xsl:template match="/book/book-body/book-part/body/sec">
    <xsl:variable name="title">
      <xsl:value-of select="translate(title, ' ', '')"/>
    </xsl:variable>
    <div class="tabs-panel" id="{$title}">
      <div class="row">
        <div class="columns">
          <xsl:choose>
            <xsl:when test="sec[@disp-level = '2']/title">
              <xsl:attribute name="class">large-9 medium-12 small-12</xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="class">large-9 medium-12 small-12</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:apply-templates/>
        </div>
        
          <div class="large-3 medium-12 small-12 columns rightrail">
            <xsl:if test="sec[@disp-level = '2']/title">
            <ul class="accordion" data-accordion="" data-allow-all-closed="true" data-multi-expand="true">
              <li class="accordion-item is-active" data-accordion-item="" aria-expanded="true" aria-selected="true">
                <a href="#" class="accordion-title" aria-controls="wp9ekv-accordion" role="tab" id="on-this-page" aria-expanded="false" aria-selected="false">Navigate this Section</a>
                <div class="accordion-content" data-tab-content="" role="tabpanel" aria-labelledby="on-this-page" aria-hidden="false" id="wp9ekv-accordion" style="display: block;">
                  <ul>
                    <xsl:for-each select="sec[@disp-level = '2']/title">
                      <xsl:variable name="sections"><xsl:value-of select="."/></xsl:variable>
                      <li><a href="#{$sections}"><xsl:value-of select="$sections"/></a></li>
                    </xsl:for-each>
                  </ul>
                </div>
              </li>
              
            </ul>
            </xsl:if>
            <xsl:call-template name="right-col-figures-tables"/>
          </div>     
      </div>
    </div>
  </xsl:template>

  <xsl:template match="/book/book-body/book-part/book-part-meta">
    <xsl:variable name="title">
      <xsl:value-of select="translate(title-group/title, ' ', '')"/>
    </xsl:variable>
    <div class="tabs-panel" id="{$title}">
      <div class="row">
          <div class="columns">
          <xsl:choose>
        <xsl:when test="body/sec[@disp-level = '2']/title">
          <xsl:attribute name="class">large-9 medium-12 small-12</xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="class">large-12 medium-12 small-12</xsl:attribute>
        </xsl:otherwise>
        </xsl:choose>
      <xsl:apply-templates/>
          </div>
        <xsl:if test="body/sec[@disp-level = '2']/title">
        <div class="large-3 medium-12 small-12 columns rightrail">
          <ul class="accordion" data-accordion="" data-allow-all-closed="true" data-multi-expand="true">
            <li class="accordion-item is-active" data-accordion-item="" aria-expanded="true" aria-selected="true">
              <a href="#" class="accordion-title" aria-controls="wp9ekv-accordion" role="tab" id="on-this-page" aria-expanded="false" aria-selected="false">Navigate this Section</a>
              <div class="accordion-content" data-tab-content="" role="tabpanel" aria-labelledby="on-this-page" aria-hidden="false" id="wp9ekv-accordion" style="display: block;">
                <ul>
                  <xsl:for-each select="body/sec[@disp-level = '2']/title">
                    <xsl:variable name="sections"><xsl:value-of select="."/></xsl:variable>
                    <li><a href="#{$sections}"><xsl:value-of select="$sections"/></a></li>
                  </xsl:for-each>
                </ul>
              </div>
            </li>
            
          </ul>
        </div>
        </xsl:if>
    </div>
    </div>
  </xsl:template>
  
  <xsl:template match="/book/book-body/book-part/back/ref-list">
    <xsl:variable name="title">
      <xsl:value-of select="translate(title, ' ', '')"/>
    </xsl:variable>
    <div class="tabs-panel" id="{$title}">
      <div class="row">
        <div class="columns">
          <xsl:choose>
            <xsl:when test="sec[@disp-level = '2']/title">
              <xsl:attribute name="class">large-9 medium-12 small-12</xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="class">large-12 medium-12 small-12</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:apply-templates/>
        </div>
        <xsl:if test="sec[@disp-level = '2']/title">
          <div class="large-3 medium-12 small-12 columns rightrail">
            <ul class="accordion" data-accordion="" data-allow-all-closed="true" data-multi-expand="true">
              <li class="accordion-item is-active" data-accordion-item="" aria-expanded="true" aria-selected="true">
                <a href="#" class="accordion-title" aria-controls="wp9ekv-accordion" role="tab" id="on-this-page" aria-expanded="false" aria-selected="false">Navigate this Section</a>
                <div class="accordion-content" data-tab-content="" role="tabpanel" aria-labelledby="on-this-page" aria-hidden="false" id="wp9ekv-accordion" style="display: block;">
                  <ul>
                    <xsl:for-each select="sec[@disp-level = '2']/title">
                      <xsl:variable name="sections"><xsl:value-of select="."/></xsl:variable>
                      <li><a href="#{$sections}"><xsl:value-of select="$sections"/></a></li>
                    </xsl:for-each>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </xsl:if>
      </div>
    </div>
  </xsl:template>
  
 <xsl:template name="right-col-figures-tables">
   <xsl:variable name="ft-id">
     <xsl:value-of select="translate(title, ' ', '')"/>
   </xsl:variable>
   <ul class="accordion" data-accordion="" data-allow-all-closed="true" data-multi-expand="true">
     <li class="accordion-item" data-accordion-item="" aria-expanded="true" aria-selected="true">
       <a href="#" class="accordion-title" aria-controls="figures-accordion" role="tab" id="figure-col" aria-expanded="false" aria-selected="false">Figures and Tables</a>
       <div class="figures-accordion-content" data-tab-content="" role="tabpanel" aria-labelledby="figure-col" aria-hidden="true" id="figures-accordion" style="display: none;">
         <ul class="tabs full-width" data-tabs="" data-active-collapse="true" id="figures-tables-{$ft-id}">
           <li class="tabs-title is-active"><a href="#figures-{$ft-id}" aria-selected="true" data-tabs-target="figures-{$ft-id}">Figures</a></li>
           <li class="tabs-title"><a href="#tables-{$ft-id}" data-tabs-target="tables-{$ft-id}">Tables</a></li>
   </ul>
         <div class="tabs-content" data-tabs-content="figures-tables-{$ft-id}">
           <div class="tabs-panel is-active" id="figures-{$ft-id}">
         <xsl:if test="/book/book-body/book-part/body//fig">
           <p><select name="figures" id="figureselector-{$ft-id}">
             <option value="">-- Select a Figure --</option>
           <xsl:for-each select="/book/book-body/book-part/body//fig">
             <xsl:variable name="label">
               <xsl:value-of select="label"/>
             </xsl:variable>
             <xsl:variable name="id">
               <xsl:value-of select="translate(label, ' ', '')"/>
             </xsl:variable>
             <xsl:variable name="caption">
               <xsl:value-of select="caption/p/text()"/>
             </xsl:variable>
             <option value="{$id}-{$ft-id}"><xsl:value-of select="$label"/>. <xsl:copy-of select="$caption"/></option>
           </xsl:for-each>
           </select></p>
           <script>
             $('#figureselector-<xsl:value-of select="$ft-id"/>').change(function(){
             $('.figurehide').hide();
             $('#' + $(this).val()).show();
             });
           </script>
         </xsl:if>
       <xsl:choose>
       <xsl:when test="/book/book-body/book-part/body//fig">
           <xsl:for-each select="/book/book-body/book-part/body//fig">
             <xsl:variable name="modal-id">
               <xsl:value-of select="@id"/>
             </xsl:variable>
       <xsl:variable name="label">
         <xsl:value-of select="label"/>
       </xsl:variable>
             <xsl:variable name="id">
               <xsl:value-of select="translate(label, ' ', '')"/>
             </xsl:variable>
       <xsl:variable name="caption">
         <xsl:value-of select="caption/p/text()"/>
       </xsl:variable>
             <xsl:variable name="caption-fn">
               <xsl:value-of select="caption/p/fn/p"/>
             </xsl:variable>
       <xsl:variable name="alt-text">
         <xsl:copy-of select="(normalize-space(string($caption)))"/>
       </xsl:variable>
       <xsl:variable name="figure">
         <xsl:for-each select="graphic/@*[namespace-uri()='http://www.w3.org/1999/xlink' and local-name()='href']">
           <xsl:value-of select="."/>
         </xsl:for-each>
       </xsl:variable>
             <p class="{$id} figurehide" id="{$id}-{$ft-id}"><strong><xsl:value-of select="$label"/>. <xsl:value-of select="$caption"/></strong><br/>
               <a data-open="basicModal-{$modal-id}">
                 <img src="img/{$figure}-thumbnail.png" class="img-framed" alt="{$alt-text}"/></a></p>
     </xsl:for-each>
   </xsl:when>
         <xsl:otherwise>
           <p>No figures present in this report.</p>
         </xsl:otherwise>
       </xsl:choose>
     </div>
           
           
           
           
           <div class="tabs-panel" id="tables-{$ft-id}">
     <xsl:if test="/book/book-meta/abstract//table-wrap | /book/book-body/book-part/body//table-wrap"> 
       <p><select name="tables" id="tableselector-{$ft-id}">
         <option value="">-- Select a Table --</option>
         <xsl:for-each select="/book/book-meta/abstract//table-wrap | /book/book-body/book-part/body//table-wrap">
           <xsl:variable name="label">
             <xsl:value-of select="label"/>
           </xsl:variable>
           <xsl:variable name="id">
             <xsl:value-of select="translate(label, ' ', '')"/>
           </xsl:variable>
           <xsl:variable name="caption">
             <xsl:copy>
               <xsl:apply-templates select="caption/title"/>
             </xsl:copy>
           </xsl:variable>
           <option value="{$id}-{$ft-id}"><xsl:value-of select="$label"/>. <xsl:value-of select="$caption"/></option>
         </xsl:for-each>
       </select></p>
       <script>
         $('#tableselector-<xsl:value-of select="$ft-id"/>').change(function(){
         $('.figurehide').hide();
         $('#' + $(this).val()).show();
         });
       </script>
     </xsl:if>
       <xsl:choose>
       <xsl:when test="/book/book-meta/abstract//table-wrap | /book/book-body/book-part/body//table-wrap">
             <xsl:for-each select="/book/book-meta/abstract//table-wrap | /book/book-body/book-part/body//table-wrap">
               <xsl:sort select="label"/>
               <xsl:variable name="label">
                 <xsl:value-of select="label"/>
               </xsl:variable>
               <xsl:variable name="id">
                 <xsl:value-of select="translate(label, ' ', '')"/>
               </xsl:variable>
               <xsl:variable name="modal-id">
                 <xsl:value-of select="@id"/>
               </xsl:variable>
               <xsl:variable name="caption">
                 <xsl:copy>
                   <xsl:apply-templates select="caption/title"/>
                 </xsl:copy>
               </xsl:variable>
               <xsl:variable name="alt-text">
                 <xsl:copy-of select="(normalize-space(string($caption)))"/>
               </xsl:variable>
               <xsl:variable name="figure">
                 <xsl:value-of select="translate(label, ' ','')"/>
               </xsl:variable>
               <p class="{$id} figurehide" id="{$id}-{$ft-id}"><strong><xsl:value-of select="$label"/>. <xsl:copy-of select="$caption"/></strong><br/>
                 <a data-open="basicModal-{$modal-id}">
                   <img src="img/{$modal-id}-thumbnail.png" class="img-framed" alt="{$alt-text}"/></a></p>
             </xsl:for-each>
     </xsl:when>
         <xsl:otherwise>
           <p>No tables present in this report.</p>
         </xsl:otherwise>
       </xsl:choose>
     </div>
   </div>
       </div>
     </li>
   </ul>
 </xsl:template>
  

  

  <xsl:template match="/book/book-back/ref-list">
    <xsl:variable name="title">
      <xsl:value-of select="translate(title, ' ', '')"/>
    </xsl:variable>
    <div class="tabs-panel" id="{$title}">
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="/book/book-body/book-part/back/app">
    <xsl:variable name="appendix">
      <xsl:value-of select="translate(label, ' ', '')"/>
    </xsl:variable>
    <xsl:if test="contains($appendix, 'Appendix')">
      <div class="tabs-panel" id="appendix">
        <h2>Appendix</h2>
        <div class="row">
          <div class="columns">
            <xsl:choose>
              <xsl:when test="sec[@disp-level = '2']/title">
                <xsl:attribute name="class">large-9 medium-12 small-12</xsl:attribute>
              </xsl:when>
              <xsl:otherwise>
                <xsl:attribute name="class">large-12 medium-12 small-12</xsl:attribute>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates/>
          </div>
          <xsl:if test="sec[@disp-level = '2']/title">
            <div class="large-3 medium-12 small-12 columns rightrail">
              <ul class="accordion" data-accordion="" data-allow-all-closed="true" data-multi-expand="true">
                <li class="accordion-item is-active" data-accordion-item="" aria-expanded="true" aria-selected="true">
                  <a href="#" class="accordion-title" aria-controls="wp9ekv-accordion" role="tab" id="on-this-page" aria-expanded="false" aria-selected="false">Navigate this Section</a>
                  <div class="accordion-content" data-tab-content="" role="tabpanel" aria-labelledby="on-this-page" aria-hidden="false" id="wp9ekv-accordion" style="display: block;">
                    <ul>
                      <xsl:for-each select="sec[@disp-level = '2']/title">
                        <xsl:variable name="sections"><xsl:value-of select="."/></xsl:variable>
                        <li><a href="#{$sections}"><xsl:value-of select="$sections"/></a></li>
                      </xsl:for-each>
                    </ul>
                  </div>
                </li>
                
              </ul>
            </div>
          </xsl:if>
        </div>
      </div>
    </xsl:if>
  </xsl:template>

  <!-- default: any other titles found -->
  <xsl:template
    match="abstract/title | /book/book-body/book-part/book-part-meta/title-group/title | /book/book-back/ref-list/title | /book/book-body/book-part/back/ref-list/title | /book/book-body/book-part/body/sec/title">
    <xsl:if test="normalize-space(string(.))">
      <h2>
        <xsl:apply-templates/>
      </h2>
    </xsl:if>
  </xsl:template>

  <xsl:template match="subtitle | sec[@disp-level = '4']/title  | book-app/book-part-meta/title-group/title">
    <xsl:if test="normalize-space(string(.))">
      <h5 class="subtitle">
        <xsl:apply-templates/>
      </h5>
    </xsl:if>
  </xsl:template>


  <!-- ============================================================= -->
  <!--  Figures, lists and block-level objects                       -->
  <!-- ============================================================= -->


  <xsl:template match="address">
    <xsl:choose>
      <!-- address appears as a sequence of inline elements if
           it has no addr-line and the parent may contain text -->
      <xsl:when
        test="
          not(addr-line) and
          (parent::collab | parent::p | parent::license-p |
          parent::named-content | parent::styled-content)">
        <xsl:call-template name="address-line"/>
      </xsl:when>
      <xsl:otherwise>
        <div class="address">
          <xsl:apply-templates/>
        </div>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>


  <xsl:template name="address-line">
    <!-- emits element children in a simple comma-delimited sequence -->
    <xsl:for-each select="*">
      <xsl:if test="position() &gt; 1">, </xsl:if>
      <xsl:apply-templates/>
    </xsl:for-each>
  </xsl:template>


  <xsl:template match="address/*">
    <p class="address-line">
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template
    match="
      array | disp-formula-group | fig-group |
      fn-group | license | long-desc | open-access | sig-block |
      table-wrap-foot | table-wrap-group">
    <div class="{local-name()}">
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="attrib">
    <p class="attrib">
      <xsl:apply-templates/>
    </p>
  </xsl:template>


  <xsl:template match="boxed-text | chem-struct-wrap | table-wrap | chem-struct-wrapper">
    <!-- chem-struct-wrapper is from NLM 2.3 -->
    <xsl:variable name="gi">
      <xsl:choose>
        <xsl:when test="self::chem-struct-wrapper">chem-struct-wrap</xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="local-name(.)"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <section class="fig">
      <xsl:call-template name="named-anchor"/>
      <xsl:variable name="id">
        <xsl:value-of select="@id"/>
      </xsl:variable>
      <xsl:variable name="caption">
        <xsl:value-of select="caption/title/text()"/>
      </xsl:variable>
      <xsl:variable name="alt-text">
        <xsl:copy-of select="(normalize-space(string($caption)))"/>
      </xsl:variable>
      <xsl:variable name="figure">
        <xsl:value-of select="translate(label, ' ','')"/>
      </xsl:variable>
      <div class="reveal" id="basicModal-{$id}" aria-labelledby="basicModalHeader" data-reveal="">
      <div class="{$gi} panel">
        <xsl:apply-templates select="." mode="label"/>
        <xsl:apply-templates/>
        <xsl:apply-templates mode="footnote"
          select="self::table-wrap//fn[not(ancestor::table-wrap-foot)]"/>
        <p style="margin-top: 1rem;"><a href="img/{$figure}.png" class="button utility"><i class="fa fa-download" title="Download PNG" aria-hidden="true" ></i>Download PNG</a><xsl:text>&#8198;</xsl:text><a href="img/{$figure}.tif" class="button utility"><i class="fa fa-download" title="Download TIF" aria-hidden="true"></i>Download TIFF</a></p>
        <button class="close-button" data-close="" aria-label="Close Modal" type="button"><i class="fa fa-close" aria-hidden="true"></i></button>
      </div>
</div>
    </section>
  </xsl:template>

 

<xsl:template match="fig">
    <xsl:for-each select=".">
      <xsl:call-template name="named-anchor"/>
      <xsl:variable name="id">
        <xsl:value-of select="@id"/>
      </xsl:variable>
      <xsl:variable name="label">
        <xsl:value-of select="label"/>
      </xsl:variable>
      <xsl:variable name="caption">
        <xsl:value-of select="caption/p/text()"/>
      </xsl:variable>
      <xsl:variable name="alt-text">
        <xsl:copy-of select="(normalize-space(string($caption)))"/>
      </xsl:variable>
      <xsl:variable name="caption-fn">
        <xsl:value-of select="caption/p/fn/p"/>
      </xsl:variable>
      <xsl:variable name="figure">
        <xsl:for-each select="graphic/@*[namespace-uri()='http://www.w3.org/1999/xlink' and local-name()='href']">
          <xsl:value-of select="."/>
        </xsl:for-each>
      </xsl:variable>
      <section class="fig">
        <div class="reveal" id="basicModal-{$id}" aria-labelledby="basicModalHeader" data-reveal="">
          <p class="h5"><xsl:value-of  select="$label"/>. <xsl:copy-of select="$caption"/></p>
          <img src="img/{$figure}.png" class="img-framed" alt="{$alt-text}"/>
          <p style="margin-top: 1rem;"><xsl:copy-of select="$caption-fn"/></p>
          <p style="margin-top: 1rem;"><a href="img/{$figure}.png" class="button utility"><i class="fa fa-download" title="Download PNG" aria-hidden="true" ></i>Download PNG</a><xsl:text>&#8198;</xsl:text><a href="img/{$figure}.tif" class="button utility"><i class="fa fa-download" title="Download TIF" aria-hidden="true"></i>Download TIFF</a></p>
          <button class="close-button" data-close="" aria-label="Close Modal" type="button"><i class="fa fa-close" aria-hidden="true"></i></button>
        </div>
      </section>
      
    </xsl:for-each>
  </xsl:template>
  
  
  <xsl:template match="disp-formula | statement">
    <div class="{local-name()} panel">
      <xsl:call-template name="named-anchor"/>
      <xsl:apply-templates select="." mode="label"/>
      <xsl:apply-templates/>
    </div>
  </xsl:template>


  <xsl:template match="glossary | gloss-group">
    <!-- gloss-group is from 2.3 -->
    <div class="glossary">
      <xsl:call-template name="named-anchor"/>
      <xsl:apply-templates select="label | title"/>
      <xsl:if test="not(normalize-space(string(title)))">
        <xsl:call-template name="block-title">
          <xsl:with-param name="contents">
            <span class="generated">Glossary</span>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:if>
      <xsl:apply-templates select="*[not(self::label | self::title)]"/>
    </div>
  </xsl:template>


  <xsl:template match="textual-form">
    <p class="textual-form">
      <span class="generated">[Textual form] </span>
      <xsl:apply-templates/>
    </p>
  </xsl:template>



  <xsl:template match="glossary/glossary | gloss-group/gloss-group">
    <!-- the same document shouldn't have both types -->
    <div class="glossary">
      <xsl:call-template name="named-anchor"/>
      <xsl:apply-templates/>
    </div>
  </xsl:template>



  <xsl:template match="inline-graphic">
    <xsl:apply-templates/>
    <img alt="{@xlink:href}">
      <xsl:call-template name="assign-src"/>
    </img>
  </xsl:template>


  <xsl:template match="alt-text">
    <!-- handled with graphic or inline-graphic -->
  </xsl:template>


  <xsl:template match="list">
    <div class="list">
      <xsl:call-template name="named-anchor"/>
      <xsl:apply-templates select="label | title"/>
      <xsl:apply-templates select="." mode="list"/>
    </div>
  </xsl:template>


  <xsl:template priority="2" mode="list" match="list[@list-type = 'simple' or list-item/label]">
    <ul style="list-style-type: none">
      <xsl:apply-templates select="list-item"/>
    </ul>
  </xsl:template>


  <xsl:template match="list[@list-type = 'bullet' or not(@list-type)]" mode="list">
    <ul>
      <xsl:apply-templates select="list-item"/>
    </ul>
  </xsl:template>


  <xsl:template match="list" mode="list">
    <xsl:variable name="style">
      <xsl:choose>
        <xsl:when test="@list-type = 'alpha-lower'">lower-alpha</xsl:when>
        <xsl:when test="@list-type = 'alpha-upper'">upper-alpha</xsl:when>
        <xsl:when test="@list-type = 'roman-lower'">lower-roman</xsl:when>
        <xsl:when test="@list-type = 'roman-upper'">upper-roman</xsl:when>
        <xsl:otherwise>decimal</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <ol style="list-style-type: {$style}">
      <xsl:apply-templates select="list-item"/>
    </ol>
  </xsl:template>


  <xsl:template match="list-item">
    <li>
      <xsl:apply-templates/>
    </li>
  </xsl:template>


  <xsl:template match="list-item/label">
    <!-- if the next sibling is a p, the label will be called as
	       a run-in -->
    <xsl:if test="following-sibling::*[1][not(self::p)]">
      <xsl:call-template name="label"/>
    </xsl:if>
  </xsl:template>


  <xsl:template match="media">
    <a>
      <xsl:call-template name="assign-id"/>
      <xsl:call-template name="assign-href"/>
      <xsl:apply-templates/>
    </a>
  </xsl:template>
  
  
  
  <xsl:template match="p[@content-type='collab-affiliation'] | p[@content-type='contrib-affiliation']">
    <br/><strong>
      <xsl:apply-templates/>
    </strong>
  </xsl:template>
  
  <xsl:template match="p[@content-type='peer-reviewer'] | p[@content-type='author-affiliation']">
    
      <xsl:choose>
        <xsl:when test="count(preceding-sibling::*) = last()">
          <strong>
            <xsl:apply-templates/>
          </strong><br/>
        </xsl:when>
        <xsl:otherwise>
          <br/><strong>
            <xsl:apply-templates/>
          </strong><br/>
        </xsl:otherwise>
      </xsl:choose>
  
  </xsl:template>
  
  <xsl:template match="p[@content-type='collab-contributions'] | p[@content-type='contrib-contributions']">
    <xsl:choose>
      <xsl:when test="count(preceding-sibling::*) = last()">
        <em>
          <xsl:apply-templates/>
        </em><br/>
      </xsl:when>
      <xsl:otherwise>
        <br/><em>
          <xsl:apply-templates/>
        </em><br/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  
  <xsl:template match="p[@content-type='collaborator'] | p[@content-type='contributor'] | p[@content-type='peer-affiliation'] | p[@content-type='author']">
    <xsl:apply-templates/><br/>
  </xsl:template>
  
  
  
  <xsl:template match="p | license-p">
    <p>
      <xsl:apply-templates/>
    </p>
  </xsl:template>
  
  <xsl:template match="list-item/p[not(preceding-sibling::*[not(self::label)])]">
    <p>
      <xsl:call-template name="assign-id"/>
      <xsl:for-each select="preceding-sibling::label">
        <span class="label">
          <xsl:apply-templates/>
        </span>
        <xsl:text> </xsl:text>
      </xsl:for-each>
      <xsl:apply-templates select="@content-type"/>
      <xsl:apply-templates/>
    </p>
  </xsl:template>


  <xsl:template match="product">
    <p class="product">
      <xsl:call-template name="assign-id"/>
      <xsl:apply-templates/>
    </p>
  </xsl:template>


  <xsl:template match="permissions">
    <div class="permissions">
      <xsl:apply-templates select="copyright-statement"/>
      <xsl:if test="copyright-year | copyright-holder">
        <p class="copyright">
          <span class="generated">Copyright</span>
          <xsl:for-each select="copyright-year | copyright-holder">
            <xsl:apply-templates/>
            <xsl:if test="not(position() = last())">
              <span class="generated">, </span>
            </xsl:if>
          </xsl:for-each>
        </p>
      </xsl:if>
      <xsl:apply-templates select="license"/>
    </div>
  </xsl:template>


  <xsl:template match="copyright-statement">
    <p class="copyright">
      <xsl:apply-templates/>
    </p>
  </xsl:template>


  <xsl:template match="def-list">
    <div class="def-list">
      <xsl:call-template name="named-anchor"/>
      <xsl:apply-templates select="label | title"/>
      <div class="def-list table">
        <xsl:if test="term-head | def-head">
          <div class="row">
            <div class="cell def-list-head">
              <xsl:apply-templates select="term-head"/>
            </div>
            <div class="cell def-list-head">
              <xsl:apply-templates select="def-head"/>
            </div>
          </div>
        </xsl:if>
        <xsl:apply-templates select="def-item"/>
      </div>
      <xsl:apply-templates select="def-list"/>
    </div>
  </xsl:template>


  <xsl:template match="def-item">
    <div class="def-item row">
      <xsl:call-template name="assign-id"/>
      <xsl:apply-templates/>
    </div>
  </xsl:template>


  <xsl:template match="term">
    <div class="def-term cell">
      <xsl:call-template name="assign-id"/>
      <p>
        <xsl:apply-templates/>
      </p>
    </div>
  </xsl:template>


  <xsl:template match="def">
    <div class="def-def cell">
      <xsl:call-template name="assign-id"/>
      <xsl:apply-templates/>
    </div>
  </xsl:template>


  <xsl:template match="disp-quote">
    <div class="blockquote">
      <xsl:call-template name="named-anchor"/>
      <xsl:apply-templates/>
    </div>
  </xsl:template>


  <xsl:template match="preformat">
    <pre class="preformat">
      <xsl:apply-templates/>
    </pre>
  </xsl:template>


  <xsl:template match="alternatives | name-alternatives | collab-alternatives | aff-alternatives">
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="citation-alternatives" priority="2">
    <!-- priority bumped to supersede match on ref/* -->
    <!-- may appear in license-p, p, ref, td, th, title  -->
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="ref-list/ref">

    <xsl:for-each select=".">
      <xsl:variable name="anchor">
        <xsl:value-of select="@id"/>
      </xsl:variable>
      <xsl:variable name="label">
        <xsl:value-of select="label"/>
        <xsl:text>. </xsl:text>
      </xsl:variable>
      <xsl:choose>
        <xsl:when test="mixed-citation[@publication-type = 'journal']">
          <p id="{$anchor}">
            <a name="{$anchor}"/>
            <xsl:if test="label">
              <xsl:value-of select="$label"/>
            </xsl:if>
            <xsl:call-template name="author-references"/>
          </p>
        </xsl:when>

        <xsl:when test="mixed-citation[@publication-type = 'book']">
          <xsl:variable name="other">
            <xsl:value-of select="mixed-citation"/>
          </xsl:variable>
          <p id="{$anchor}">
            <a name="{$anchor}"/>
            <xsl:if test="label">
              <xsl:value-of select="$label"/>
            </xsl:if>
            <xsl:choose>
              <xsl:when test="person-group"><xsl:call-template name="author-references-book"/><xsl:text>. </xsl:text></xsl:when>
              <xsl:otherwise><xsl:value-of select="$other"/></xsl:otherwise>
            </xsl:choose>
          </p>
        </xsl:when>

        <xsl:when test="mixed-citation[@publication-type = 'web']">
          <xsl:variable name="web">
            <xsl:value-of select="mixed-citation/text()"/>
            <xsl:if test="mixed-citation/ext-link[@ext-link-type='uri']">
            <xsl:variable name="uri"><xsl:copy-of select="mixed-citation/ext-link[@ext-link-type='uri']"/></xsl:variable>
            <a href="{$uri}"><xsl:value-of select="$uri"/></a>
            </xsl:if>
            <xsl:if test="mixed-citation/pub-id[@pub-id-type = 'doi']">
            <xsl:variable name="doi"><xsl:copy-of select="mixed-citation/pub-id[@pub-id-type = 'doi']"/></xsl:variable>
            <a href="http://dx.doi.org/{$doi}" target="_blank"><xsl:value-of select="$doi"/></a>
            </xsl:if>
          </xsl:variable>
          <p id="{$anchor}">
            <a name="{$anchor}"/>
            <xsl:if test="label">
              <xsl:value-of select="$label"/>
            </xsl:if>
            <xsl:copy-of select="$web"/>
          </p>
        </xsl:when>

        <xsl:when test="mixed-citation[@publication-type = 'other'] | mixed-citation[@publication-type = 'confproc']">
          <xsl:variable name="other">
            <xsl:value-of select="mixed-citation"/>
          </xsl:variable>
          <p id="{$anchor}">
            <a name="{$anchor}"/>
            <xsl:if test="label">
              <xsl:value-of select="$label"/>
            </xsl:if>
            <xsl:value-of select="$other"/>
          </p>
        </xsl:when>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="author-references">
    <xsl:variable name="name">
      <xsl:for-each select="mixed-citation/person-group/name">
        <xsl:if test="position() > 1">
          <xsl:choose>
            <xsl:when test="position() = last()">, </xsl:when>
            <xsl:otherwise>, </xsl:otherwise>
          </xsl:choose>
        </xsl:if>
        <xsl:value-of select="surname"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="given-names"/>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="id">
      <xsl:value-of select="mixed-citation/article-title"/>
    </xsl:variable>
    <xsl:variable name="year">
      <xsl:value-of select="mixed-citation/year"/>
      <xsl:text>;</xsl:text>
    </xsl:variable>
    <xsl:variable name="article">
      <xsl:value-of select="mixed-citation/article-title"/>
    </xsl:variable>
    <xsl:variable name="source">
      <xsl:value-of select="mixed-citation/source"/>
      <xsl:text>.</xsl:text>
    </xsl:variable>
    <xsl:variable name="volume">
      <xsl:value-of select="mixed-citation/volume"/>
    </xsl:variable>
    <xsl:variable name="issue">
      <xsl:text>(</xsl:text>
      <xsl:value-of select="mixed-citation/issue"/>
      <xsl:text>)</xsl:text>
    </xsl:variable>
    <xsl:variable name="fpage">
      <xsl:value-of select="mixed-citation/fpage"/>
    </xsl:variable>
    <xsl:variable name="lpage">
      <xsl:text>-</xsl:text>
      <xsl:value-of select="mixed-citation/lpage"/>
    </xsl:variable> 
    <xsl:variable name="doi">
      <xsl:value-of select="mixed-citation/pub-id[@pub-id-type = 'doi']"/>
    </xsl:variable>
    <xsl:variable name="uri">
      <xsl:value-of select="mixed-citation/ext-link[@ext-link-type = 'uri']"/>
    </xsl:variable>
    <xsl:variable name="pmid">
      <xsl:value-of select="mixed-citation/pub-id[@pub-id-type = 'pmid']"/>
    </xsl:variable>
    <xsl:variable name="collab">
      <xsl:value-of select="mixed-citation/person-group/collab"/>
    </xsl:variable>
    <xsl:if test="mixed-citation/person-group/collab">
      <xsl:value-of select="$collab"/>
    </xsl:if>
    <xsl:value-of select="$name"/>
    <xsl:text>. </xsl:text>
    <xsl:value-of select="$article"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="$source"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="$year"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="$volume"/>
    <xsl:if test="mixed-citation/issue"><xsl:value-of select="$issue"/></xsl:if>
    <xsl:text>:</xsl:text>
    <xsl:value-of select="$fpage"/>
    <xsl:if test="mixed-citation/lpage"><xsl:value-of select="$lpage"/></xsl:if>
    <xsl:text>.</xsl:text>
    <xsl:if test="mixed-citation/pub-id[@pub-id-type = 'doi']">
      <xsl:text> </xsl:text>
      <a href="http://dx.doi.org/{$doi}" target="_blank"><xsl:value-of select="$doi"/></a>
    </xsl:if>
    <xsl:if test="mixed-citation/ext-link[@ext-link-type = 'uri']">
      <xsl:text> </xsl:text>
      <a href="{$uri}" target="_blank"><xsl:value-of select="$uri"/></a>
    </xsl:if>
    <xsl:if test="mixed-citation/pub-id[@pub-id-type = 'pmid']">
    <xsl:text> | </xsl:text>
    <a href="https://www.ncbi.nlm.nih.gov/pubmed/{$pmid}">PubMed</a>
    </xsl:if>
  </xsl:template>

  <xsl:template name="author-references-book">
    <xsl:variable name="name">
      <xsl:for-each select="mixed-citation/person-group/name">
        <xsl:if test="position() > 1">
          <xsl:choose>
            <xsl:when test="position() = last()">, </xsl:when>
            <xsl:otherwise>, </xsl:otherwise>
          </xsl:choose>
        </xsl:if>
        <xsl:value-of select="surname"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="given-names"/>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="source">
      <xsl:value-of select="mixed-citation/source"/>
    </xsl:variable>
    <xsl:variable name="year">
      <xsl:value-of select="mixed-citation/year"/>
    </xsl:variable>
    <xsl:variable name="publisher-loc">
      <xsl:value-of select="mixed-citation/publisher-loc"/>
    </xsl:variable>
    <xsl:variable name="publisher-name">
      <xsl:text>(</xsl:text>
      <xsl:value-of select="mixed-citation/publisher-name"/>
      <xsl:text>)</xsl:text>
    </xsl:variable>
    <xsl:variable name="collab">
      <xsl:value-of select="mixed-citation/person-group/collab"/>
    </xsl:variable>
    <xsl:if test="mixed-citation/person-group/collab">
      <xsl:value-of select="$collab"/>
    </xsl:if>
    <xsl:value-of select="$name"/>
    <xsl:text>. </xsl:text>
    <xsl:value-of select="$source"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="$publisher-loc"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="$publisher-name"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="$year"/>
    <xsl:text>.</xsl:text>
  </xsl:template>


  
  <xsl:template match="ref/* | ref/citation-alternatives/*" priority="0">
    <!-- should match mixed-citation, element-citation, nlm-citation,
         or citation (in 2.3); note and label should be matched below;
         citation-alternatives is handled elsewhere -->
    <p class="citation">
      <xsl:call-template name="named-anchor"/>
      <xsl:apply-templates/>
    </p>
  </xsl:template>


  <xsl:template match="ref/note" priority="2">
    <xsl:param name="label" select="''"/>
    <xsl:if
      test="
        normalize-space(string($label))
        and not(preceding-sibling::*[not(self::label)])">
      <p class="label">
        <xsl:copy-of select="$label"/>
      </p>
    </xsl:if>
    <div class="note">
      <xsl:call-template name="named-anchor"/>
      <xsl:apply-templates/>
    </div>
  </xsl:template>


  <xsl:template
    match="
      app/related-article |
      app-group/related-article | bio/related-article |
      body/related-article | boxed-text/related-article |
      disp-quote/related-article | glossary/related-article |
      gloss-group/related-article |
      ref-list/related-article | sec/related-article">
    <xsl:apply-templates select="." mode="metadata"/>
  </xsl:template>


  <xsl:template
    match="
      app/related-object |
      app-group/related-object | bio/related-object |
      body/related-object | boxed-text/related-object |
      disp-quote/related-object | glossary/related-object |
      gloss-group/related-article |
      ref-list/related-object | sec/related-object">
    <xsl:apply-templates select="." mode="metadata"/>
  </xsl:template>


  <xsl:template match="speech">
    <div class="speech">
      <xsl:call-template name="named-anchor"/>
      <xsl:apply-templates mode="speech"/>
    </div>
  </xsl:template>


  <xsl:template match="speech/speaker" mode="speech"/>


  <xsl:template match="speech/p" mode="speech">
    <p>
      <xsl:apply-templates select="self::p[not(preceding-sibling::p)]/../speaker"/>
      <xsl:apply-templates/>
    </p>
  </xsl:template>


  <xsl:template match="speech/speaker">
    <b>
      <xsl:apply-templates/>
    </b>
    <span class="generated">: </span>
  </xsl:template>


  <xsl:template match="supplementary-material">
    <div class="panel">
      <xsl:call-template name="named-anchor"/>
      <xsl:apply-templates select="." mode="label"/>
      <xsl:apply-templates/>
    </div>
  </xsl:template>


  <xsl:template match="tex-math">
    <span class="tex-math">
      <span class="generated">[TeX:] </span>
      <xsl:apply-templates/>
    </span>
  </xsl:template>


  <xsl:template match="mml:*">
    <!-- this stylesheet simply copies MathML through. If your browser
         supports it, you will get it -->
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>


  <xsl:template match="verse-group">
    <div class="verse">
      <xsl:call-template name="named-anchor"/>
      <xsl:apply-templates/>
    </div>
  </xsl:template>


  <xsl:template match="verse-line">
    <p class="verse-line">
      <xsl:apply-templates/>
    </p>
  </xsl:template>




  <xsl:template
    match="
      app/label | boxed-text/label |
      chem-struct-wrap/label | chem-struct-wrapper/label |
      disp-formula/label | fig/label | fn/label | ref/label |
      statement/label | supplementary-material/label | table-wrap/label"
    priority="2">
    <!-- suppressed, since acquired by their parents in mode="label" -->
  </xsl:template>


  <xsl:template match="p/label">
    <span class="label">
      <xsl:apply-templates/>
    </span>
  </xsl:template>

  <xsl:template match="label" name="label">
    <!-- other labels are displayed as blocks -->
    <p class="h5">
      <xsl:apply-templates/>
    </p>
  </xsl:template>


  <!-- ============================================================= -->
  <!--  TABLES                                                       -->
  <!-- ============================================================= -->
  <!--  Tables are already in XHTML, and can simply be copied
        through                                                      -->


  <xsl:template match="table-wrap/caption">
      <xsl:copy>
        <xsl:apply-templates select="table-wrap/caption/@*"/>
        <span class="h5"><xsl:apply-templates/></span>
      </xsl:copy>
  </xsl:template>
  
  <xsl:template match="table-wrap/label">
    <xsl:copy>
      <xsl:apply-templates select="table-wrap/label/@*"/>
      <span class="h5"><xsl:apply-templates/></span>
    </xsl:copy>
  </xsl:template>


  <xsl:template match="
      table | thead | tbody | tfoot | tr | th | td">
    <xsl:copy>
      <xsl:apply-templates select="@*" mode="table-copy"/>
      <xsl:call-template name="named-anchor"/>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>


  <xsl:template match="array/tbody">
    <table>
      <xsl:copy>
        <xsl:apply-templates select="@*" mode="table-copy"/>
        <xsl:call-template name="named-anchor"/>
        <xsl:apply-templates/>
      </xsl:copy>
    </table>
  </xsl:template>


  <xsl:template match="@*" mode="table-copy">
    <xsl:copy-of select="."/>
  </xsl:template>


  <xsl:template match="@content-type" mode="table-copy"/>






  <!-- ============================================================= -->
  <!--  INLINE MISCELLANEOUS                                         -->
  <!-- ============================================================= -->
  <!--  Templates strictly for formatting follow; these are templates
        to handle various inline structures -->


  <xsl:template match="abbrev">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="abbrev[normalize-space(string(@xlink:href))]">
    <a>
      <xsl:call-template name="assign-href"/>
      <xsl:apply-templates/>
    </a>
  </xsl:template>

  <xsl:template match="abbrev/def">
    <xsl:text>[</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>]</xsl:text>
  </xsl:template>

  <xsl:template
    match="
      p/address | license-p/address |
      named-content/p | styled-content/p">
    <xsl:apply-templates mode="inline"/>
  </xsl:template>


  <xsl:template match="address/*" mode="inline">
    <xsl:if test="preceding-sibling::*">
      <span class="generated">, </span>
    </xsl:if>
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="award-id">
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="award-id[normalize-space(string(@rid))]">
    <a href="#{@rid}">
      <xsl:apply-templates/>
    </a>
  </xsl:template>


  <xsl:template match="break">
    <br class="br"/>
  </xsl:template>


  <xsl:template match="email">
    <a href="mailto:{.}">
      <xsl:apply-templates/>
    </a>
  </xsl:template>


  <xsl:template match="ext-link | uri | inline-supplementary-material">
    <a target="xrefwindow">
      <xsl:attribute name="href">
        <xsl:value-of select="."/>
      </xsl:attribute>
      <!-- if an @href is present, it overrides the href
           just attached -->
      <xsl:call-template name="assign-href"/>
      <xsl:call-template name="assign-id"/>
      <xsl:apply-templates/>
      <xsl:if test="not(normalize-space(string(.)))">
        <xsl:value-of select="@xlink:href"/>
      </xsl:if>
    </a>
  </xsl:template>


  <xsl:template match="funding-source">
    <span class="funding-source">
      <xsl:apply-templates/>
    </span>
  </xsl:template>


  <xsl:template match="hr">
    <hr class="hr"/>
  </xsl:template>


  <!-- inline-graphic is handled above, with graphic -->


  <xsl:template match="inline-formula | chem-struct">
    <span class="{local-name()}">
      <xsl:apply-templates/>
    </span>
  </xsl:template>


  <xsl:template match="chem-struct-wrap/chem-struct | chem-struct-wrapper/chem-struct">
    <div class="{local-name()}">
      <xsl:apply-templates/>
    </div>
  </xsl:template>


  <xsl:template match="milestone-start | milestone-end">
    <span class="{substring-after(local-name(),'milestone-')}">
      <xsl:comment>
        <xsl:value-of select="@rationale"/>
      </xsl:comment>
    </span>
  </xsl:template>


  <xsl:template match="object-id">
    <span class="{local-name()}">
      <xsl:apply-templates/>
    </span>
  </xsl:template>


  <!-- preformat is handled above -->

  <xsl:template match="sig">
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="target">
    <xsl:call-template name="named-anchor"/>
  </xsl:template>


  <xsl:template match="styled-content">
    <span>
      <xsl:copy-of select="@style"/>
      <xsl:for-each select="@style-type">
        <xsl:attribute name="class">
          <xsl:value-of select="."/>
        </xsl:attribute>
      </xsl:for-each>
      <xsl:apply-templates/>
    </span>
  </xsl:template>


  <xsl:template match="named-content">
    <span>
      <xsl:for-each select="@content-type">
        <xsl:attribute name="class">
          <xsl:value-of select="translate(., ' ', '-')"/>
        </xsl:attribute>
      </xsl:for-each>
      <xsl:apply-templates/>
    </span>
  </xsl:template>





  <xsl:template match="glyph-data | glyph-ref">
    <span class="generated">(Glyph not rendered)</span>
  </xsl:template>


  <xsl:template match="related-article">
    <span class="generated">[Related article:] </span>
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="related-object">
    <span class="generated">[Related object:] </span>
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="xref[@ref-type = 'bibr']">
    <a class="ref" href="#{@rid}">
      <span tip="#{@rid}" data-tooltip="" aria-haspopup="true" class="has-tip" data-allow-html="true" data-disable-hover="false" tabindex="0">
      <xsl:apply-templates/>
      </span>
    </a>
  </xsl:template>

  <xsl:template match="xref[not(normalize-space(string(.)))]">
    <a href="#{@rid}">
      <xsl:apply-templates select="key('element-by-id', @rid)" mode="label-text">
        <xsl:with-param name="warning" select="true()"/>
      </xsl:apply-templates>
    </a>
  </xsl:template>


 <xsl:template match="xref[@ref-type = 'table'] | xref[@ref-type = 'fig']">
    <a data-open="basicModal-{@rid}">
      <xsl:apply-templates/>
    </a>
  </xsl:template>

  <xsl:template match="xref">
    <a href="#{@rid}">
      <xsl:apply-templates/>
    </a>
  </xsl:template>





  <!-- ============================================================= -->
  <!--  Formatting elements                                          -->
  <!-- ============================================================= -->


  <xsl:template match="bold">
    <b>
      <xsl:apply-templates/>
    </b>
  </xsl:template>
  
  <xsl:template match="p[@content-type='bold']">
    <b>
      <xsl:apply-templates/>
    </b>
  </xsl:template>

  
  <xsl:template match="italic">
    <i>
      <xsl:apply-templates/>
    </i>
  </xsl:template>


  <xsl:template match="monospace">
    <tt>
      <xsl:apply-templates/>
    </tt>
  </xsl:template>


  <xsl:template match="overline">
    <span style="text-decoration: overline">
      <xsl:apply-templates/>
    </span>
  </xsl:template>


  <xsl:template match="price">
    <span class="price">
      <xsl:apply-templates/>
    </span>
  </xsl:template>


  <xsl:template match="roman">
    <span style="font-style: normal">
      <xsl:apply-templates/>
    </span>
  </xsl:template>


  <xsl:template match="sans-serif">
    <span style="font-family: sans-serif; font-size: 80%">
      <xsl:apply-templates/>
    </span>
  </xsl:template>


  <xsl:template match="sc">
    <span style="font-variant: small-caps">
      <xsl:apply-templates/>
    </span>
  </xsl:template>


  <xsl:template match="strike">
    <span style="text-decoration: line-through">
      <xsl:apply-templates/>
    </span>
  </xsl:template>


  <xsl:template match="sub">
    <sub>
      <xsl:apply-templates/>
    </sub>
  </xsl:template>




  <xsl:template match="xref[@ref-type = 'bibr']/sup">
    <sup>
      <xsl:apply-templates/>
    </sup>
  </xsl:template>
  
  <xsl:template match="sup">
    <sup>
      <xsl:apply-templates/>
    </sup>
  </xsl:template>


  <xsl:template match="underline">
    <span style="text-decoration: underline">
      <xsl:apply-templates/>
    </span>
  </xsl:template>



  <!-- ============================================================= -->
  <!--  BACK MATTER                                                  -->
  <!-- ============================================================= -->


  <xsl:variable name="loose-footnotes"
    select="//fn[not(ancestor::front | parent::fn-group | ancestor::table-wrap)]"/>


  <xsl:template name="make-back">
    <xsl:apply-templates select="back"/>
    <xsl:if test="$loose-footnotes and not(back)">
      <!-- autogenerating a section for footnotes only if there is no
           back element, and if footnotes exist for it -->
      <xsl:call-template name="footnotes"/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="back">
    <!-- content model for back: 
          (label?, title*, 
          (ack | app-group | bio | fn-group | glossary | ref-list |
           notes | sec)*) -->
    <xsl:if test="not(fn-group) and $loose-footnotes">
      <xsl:call-template name="footnotes"/>
    </xsl:if>
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template name="footnotes">
    <xsl:call-template name="backmatter-section">
      <xsl:with-param name="generated-title">Notes</xsl:with-param>
      <xsl:with-param name="contents">
        <xsl:apply-templates select="$loose-footnotes" mode="footnote"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="ack">
    <xsl:call-template name="backmatter-section">
      <xsl:with-param name="generated-title">Acknowledgements</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="app-group">
    <xsl:call-template name="backmatter-section">
      <xsl:with-param name="generated-title">Appendices</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="back/bio">
    <xsl:call-template name="backmatter-section">
      <xsl:with-param name="generated-title">Biography</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="back/fn-group">
    <xsl:call-template name="backmatter-section">
      <xsl:with-param name="generated-title">Notes</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="back/glossary | back/gloss-group">
    <xsl:call-template name="backmatter-section">
      <xsl:with-param name="generated-title">Glossary</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


 <!-- <xsl:template match="back/ref-list">
    <xsl:call-template name="backmatter-section">
      <xsl:with-param name="generated-title">References</xsl:with-param>
      <xsl:with-param name="contents">
        <div class="tabs-panel" id="references">
          <xsl:call-template name="ref-list"/>
        </div>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template> -->


  <xsl:template match="back/notes">
    <xsl:call-template name="backmatter-section">
      <xsl:with-param name="generated-title">Notes</xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template name="backmatter-section">
    <!--<xsl:param name="generated-title"/>
    <xsl:param name="contents">
      <xsl:apply-templates/>
    </xsl:param>
    <div class="back-section">
      <xsl:call-template name="named-anchor"/>
      <xsl:if test="not(title) and $generated-title">
        <xsl:choose>
          <xsl:when test="ancestor::back/title">
            <xsl:call-template name="section-title">
              <xsl:with-param name="contents" select="$generated-title"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="main-title">
              <xsl:with-param name="contents" select="$generated-title"/>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
      <xsl:copy-of select="$contents"/>
    </div>-->
  </xsl:template>


  <!-- ============================================================= -->
  <!--  FOOTNOTES                                                    -->
  <!-- ============================================================= -->

  <!--<xsl:template match="fn">
    <xsl:variable name="id">
      <xsl:apply-templates select="." mode="id"/>
    </xsl:variable>
    <a href="#{$id}">
      <xsl:apply-templates select="." mode="label-text">
        <xsl:with-param name="warning" select="true()"/>
      </xsl:apply-templates>
    </a>
  </xsl:template>-->

  <xsl:template match="
      fn-group/fn | table-wrap-foot/fn |
      table-wrap-foot/fn-group/fn">
    <small><xsl:apply-templates select="." mode="footnote"/></small><br/>
  </xsl:template>


  <xsl:template match="fn/label/sup" mode="footnote">
    <sup>
      <xsl:value-of select="."/>
    </sup>
    <xsl:text> </xsl:text>
  </xsl:template>

  <xsl:template match="fn/p/email" mode="footnote">
    <xsl:variable name="email"><xsl:value-of select="@xlink:href"/></xsl:variable>
<a href="mailto:{$email}"><xsl:value-of select="$email"/></a>
  </xsl:template>


  <!-- ============================================================= -->
  <!--  MODE 'label-text'
	      Generates label text for elements and their cross-references -->
  <!-- ============================================================= -->


  <xsl:variable name="auto-label-app" select="false()"/>
  <xsl:variable name="auto-label-boxed-text" select="false()"/>
  <xsl:variable name="auto-label-chem-struct-wrap" select="false()"/>
  <xsl:variable name="auto-label-disp-formula" select="false()"/>
  <xsl:variable name="auto-label-fig" select="false()"/>
  <xsl:variable name="auto-label-ref" select="not(//ref[label])"/>
  <!-- ref elements are labeled unless any ref already has a label -->

  <xsl:variable name="auto-label-statement" select="false()"/>
  <xsl:variable name="auto-label-supplementary" select="false()"/>
  <xsl:variable name="auto-label-table-wrap" select="false()"/>

  <!--
  These variables assignments show how autolabeling can be 
  configured conditionally.
  For example: "label figures if no figures have labels" translates to
    "not(//fig[label])", which will resolve to Boolean true() when the set of
    all fig elements with labels is empty.
  
  <xsl:variable name="auto-label-app" select="not(//app[label])"/>
  <xsl:variable name="auto-label-boxed-text" select="not(//boxed-text[label])"/>
  <xsl:variable name="auto-label-chem-struct-wrap" select="not(//chem-struct-wrap[label])"/>
  <xsl:variable name="auto-label-disp-formula" select="not(//disp-formula[label])"/>
  <xsl:variable name="auto-label-fig" select="not(//fig[label])"/>
  <xsl:variable name="auto-label-ref" select="not(//ref[label])"/>
  <xsl:variable name="auto-label-statement" select="not(//statement[label])"/>
  <xsl:variable name="auto-label-supplementary"
    select="not(//supplementary-material[not(ancestor::front)][label])"/>
  <xsl:variable name="auto-label-table-wrap" select="not(//table-wrap[label])"/>
-->


  <xsl:template mode="label" match="*" name="block-label">
    <xsl:param name="contents">
      <xsl:apply-templates select="." mode="label-text">
        <xsl:with-param name="warning"
          select="boolean(key('xref-by-rid', @id)[not(normalize-space(string(.)))])"/>
      </xsl:apply-templates>
    </xsl:param>
    <xsl:if test="normalize-space(string($contents))">
      <span class="h5">
        <xsl:copy-of select="$contents"/><xsl:text>. </xsl:text>
      </span>
    </xsl:if>
  </xsl:template>



  <xsl:template mode="label" match="ref">
    <xsl:param name="contents">
      <xsl:apply-templates select="." mode="label-text"/>
    </xsl:param>
    <xsl:if test="normalize-space(string($contents))">
      <!-- we're already in a p -->
      <span class="label">
        <xsl:copy-of select="$contents"/>
      </span>
    </xsl:if>
  </xsl:template>


  <xsl:template match="app" mode="label-text">
    <xsl:param name="warning" select="true()"/>
    <!-- pass $warning in as false() if a warning string is not wanted
         (for example, if generating autonumbered labels) -->
    <xsl:call-template name="make-label-text">
      <xsl:with-param name="auto" select="$auto-label-app"/>
      <xsl:with-param name="warning" select="$warning"/>
      <!--
      Pass in the desired label if a label is to be autogenerated  
      <xsl:with-param name="auto-text">
        <xsl:text>Appendix </xsl:text>
        <xsl:number format="A"/>
      </xsl:with-param>-->
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="boxed-text" mode="label-text">
    <xsl:param name="warning" select="true()"/>
    <!-- pass $warning in as false() if a warning string is not wanted
         (for example, if generating autonumbered labels) -->
    <xsl:call-template name="make-label-text">
      <xsl:with-param name="auto" select="$auto-label-boxed-text"/>
      <xsl:with-param name="warning" select="$warning"/>
      <xsl:with-param name="auto-text">
        <xsl:text>Box </xsl:text>
        <xsl:number level="any"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="disp-formula" mode="label-text">
    <xsl:param name="warning" select="true()"/>
    <!-- pass $warning in as false() if a warning string is not wanted
         (for example, if generating autonumbered labels) -->
    <xsl:call-template name="make-label-text">
      <xsl:with-param name="auto" select="$auto-label-disp-formula"/>
      <xsl:with-param name="warning" select="$warning"/>
      <xsl:with-param name="auto-text">
        <xsl:text>Formula </xsl:text>
        <xsl:number level="any"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="chem-struct-wrap | chem-struct-wrapper" mode="label-text">
    <xsl:param name="warning" select="true()"/>
    <!-- pass $warning in as false() if a warning string is not wanted
         (for example, if generating autonumbered labels) -->
    <xsl:call-template name="make-label-text">
      <xsl:with-param name="auto" select="$auto-label-chem-struct-wrap"/>
      <xsl:with-param name="warning" select="$warning"/>
      <xsl:with-param name="auto-text">
        <xsl:text>Formula (chemical) </xsl:text>
        <xsl:number level="any"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="fig" mode="label-text">
    <xsl:param name="warning" select="true()"/>
    <!-- pass $warning in as false() if a warning string is not wanted
         (for example, if generating autonumbered labels) -->
    <xsl:call-template name="make-label-text">
      <xsl:with-param name="auto" select="$auto-label-fig"/>
      <xsl:with-param name="warning" select="$warning"/>
      <xsl:with-param name="auto-text">
        <xsl:text>Figure </xsl:text>
        <xsl:number level="any"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="front//fn" mode="label-text">
    <xsl:param name="warning" select="boolean(key('xref-by-rid', @id))"/>
    <!-- pass $warning in as false() if a warning string is not wanted
         (for example, if generating autonumbered labels);
         by default, we get a warning only if we need a label for
         a cross-reference -->
    <!-- auto-number-fn is true only if (1) this fn is cross-referenced, and
         (2) there exists inside the front matter any fn elements with
         cross-references, but not labels or @symbols. -->
    <xsl:param name="auto-number-fn"
      select="
        boolean(key('xref-by-rid', parent::fn/@id)) and
        boolean(ancestor::front//fn[key('xref-by-rid', @id)][not(label | @symbol)])"/>
    <xsl:call-template name="make-label-text">
      <xsl:with-param name="auto" select="$auto-number-fn"/>
      <xsl:with-param name="warning" select="$warning"/>
      <xsl:with-param name="auto-text">
        <xsl:number level="any" count="fn" from="front" format="a"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="table-wrap//fn" mode="label-text">
    <xsl:param name="warning" select="boolean(key('xref-by-rid', @id))"/>
    <!-- pass $warning in as false() if a warning string is not wanted
         (for example, if generating autonumbered labels);
         by default, we get a warning only if we need a label for
         a cross-reference -->
    <xsl:param name="auto-number-fn"
      select="not(ancestor::table-wrap//fn/label | ancestor::table-wrap//fn/@symbol)"/>
    <xsl:call-template name="make-label-text">
      <xsl:with-param name="auto" select="$auto-number-fn"/>
      <xsl:with-param name="warning" select="$warning"/>
      <xsl:with-param name="auto-text">
        <xsl:text>[</xsl:text>
        <xsl:number level="any" count="fn" from="table-wrap" format="i"/>
        <xsl:text>]</xsl:text>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="fn" mode="label-text">
    <xsl:param name="warning" select="boolean(key('xref-by-rid', @id))"/>
    <!-- pass $warning in as false() if a warning string is not wanted
         (for example, if generating autonumbered labels);
         by default, we get a warning only if we need a label for
         a cross-reference -->
    <!-- autonumber all fn elements outside fn-group,
         front matter and table-wrap only if none of them 
         have labels or @symbols (to keep numbering
         orderly) -->
    <xsl:variable name="in-scope-notes"
      select="
        ancestor::article//fn[not(parent::fn-group
        | ancestor::front
        | ancestor::table-wrap)]"/>
    <xsl:variable name="auto-number-fn"
      select="
        not($in-scope-notes/label |
        $in-scope-notes/@symbol)"/>
    <xsl:call-template name="make-label-text">
      <xsl:with-param name="auto" select="$auto-number-fn"/>
      <xsl:with-param name="warning" select="$warning"/>
      <xsl:with-param name="auto-text">
        <xsl:text>[</xsl:text>
        <xsl:number level="any" count="fn[not(ancestor::front)]"
          from="article | sub-article | response"/>
        <xsl:text>]</xsl:text>
        <xsl:apply-templates select="@fn-type"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="fn/@fn-type[. = 'abbr']" priority="2">
    <span class="generated"> Abbreviation</span>
  </xsl:template>

  <xsl:template match="fn/@fn-type[. = 'com']" priority="2">
    <span class="generated"> Communicated by</span>
  </xsl:template>

  <xsl:template match="fn/@fn-type[. = 'con']" priority="2">
    <span class="generated"> Contributed by</span>
  </xsl:template>

  <xsl:template match="fn/@fn-type[. = 'conflict']" priority="2">
    <span class="generated"> Conflicts of interest</span>
  </xsl:template>

  <xsl:template match="fn/@fn-type[. = 'corresp']" priority="2">
    <span class="generated"> Corresponding author</span>
  </xsl:template>

  <xsl:template match="fn/@fn-type[. = 'current-aff']" priority="2">
    <span class="generated"> Current affiliation</span>
  </xsl:template>

  <xsl:template match="fn/@fn-type[. = 'deceased']" priority="2">
    <span class="generated"> Deceased</span>
  </xsl:template>

  <xsl:template match="fn/@fn-type[. = 'edited-by']" priority="2">
    <span class="generated"> Edited by</span>
  </xsl:template>

  <xsl:template match="fn/@fn-type[. = 'equal']" priority="2">
    <span class="generated"> Equal contributor</span>
  </xsl:template>

  <xsl:template match="fn/@fn-type[. = 'financial-disclosure']" priority="2">
    <span class="generated"> Financial disclosure</span>
  </xsl:template>

  <xsl:template match="fn/@fn-type[. = 'on-leave']" priority="2">
    <span class="generated"> On leave</span>
  </xsl:template>

  <xsl:template match="fn/@fn-type[. = 'other']" priority="2"/>

  <xsl:template match="fn/@fn-type[. = 'participating-researchers']" priority="2">
    <span class="generated"> Participating researcher</span>
  </xsl:template>

  <xsl:template match="fn/@fn-type[. = 'present-address']" priority="2">
    <span class="generated"> Current address</span>
  </xsl:template>

  <xsl:template match="fn/@fn-type[. = 'presented-at']" priority="2">
    <span class="generated"> Presented at</span>
  </xsl:template>

  <xsl:template match="fn/@fn-type[. = 'presented-by']" priority="2">
    <span class="generated"> Presented by</span>
  </xsl:template>

  <xsl:template match="fn/@fn-type[. = 'previously-at']" priority="2">
    <span class="generated"> Previously at</span>
  </xsl:template>

  <xsl:template match="fn/@fn-type[. = 'study-group-members']" priority="2">
    <span class="generated"> Study group member</span>
  </xsl:template>

  <xsl:template match="fn/@fn-type[. = 'supplementary-material']" priority="2">
    <span class="generated"> Supplementary material</span>
  </xsl:template>

  <xsl:template match="fn/@fn-type[. = 'supported-by']" priority="2">
    <span class="generated"> Supported by</span>
  </xsl:template>

  <xsl:template match="fn/@fn-type"/>




  <xsl:template match="statement" mode="label-text">
    <xsl:param name="warning" select="true()"/>
    <!-- pass $warning in as false() if a warning string is not wanted
         (for example, if generating autonumbered labels) -->
    <xsl:call-template name="make-label-text">
      <xsl:with-param name="auto" select="$auto-label-statement"/>
      <xsl:with-param name="warning" select="$warning"/>
      <xsl:with-param name="auto-text">
        <xsl:text>Statement </xsl:text>
        <xsl:number level="any"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="supplementary-material" mode="label-text">
    <xsl:param name="warning" select="true()"/>
    <!-- pass $warning in as false() if a warning string is not wanted
         (for example, if generating autonumbered labels) -->
    <xsl:call-template name="make-label-text">
      <xsl:with-param name="auto" select="$auto-label-supplementary"/>
      <xsl:with-param name="warning" select="$warning"/>
      <xsl:with-param name="auto-text">
        <xsl:text>Supplementary Material </xsl:text>
        <xsl:number level="any" format="A" count="supplementary-material[not(ancestor::front)]"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="table-wrap" mode="label-text">
    <xsl:param name="warning" select="true()"/>
    <!-- pass $warning in as false() if a warning string is not wanted
         (for example, if generating autonumbered labels) -->
    <xsl:call-template name="make-label-text">
      <xsl:with-param name="auto" select="$auto-label-table-wrap"/>
      <xsl:with-param name="warning" select="$warning"/>
      <xsl:with-param name="auto-text">
        <xsl:text>Table </xsl:text>
        <xsl:number level="any" format="I"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="*" mode="label-text">
    <xsl:param name="warning" select="true()"/>
    <!-- pass $warning in as false() if a warning string is not wanted
         (for example, if generating autonumbered labels) -->
    <xsl:call-template name="make-label-text">
      <xsl:with-param name="warning" select="$warning"/>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="label" mode="label-text">
    <xsl:apply-templates mode="inline-label-text"/>
  </xsl:template>


  <xsl:template match="text()" mode="inline-label-text">
    <!-- when displaying labels, space characters become non-breaking spaces -->
    <xsl:value-of
      select="translate(normalize-space(string(.)), ' &#xA;&#x9;', '&#xA0;&#xA0;&#xA0;')"/>
  </xsl:template>


  <!-- ============================================================= -->
  <!--  Writing a name                                               -->
  <!-- ============================================================= -->

  <!-- Called when displaying structured names in metadata         -->

  <xsl:template match="name">
    <xsl:apply-templates select="prefix" mode="inline-name"/>
    <xsl:apply-templates select="surname[../@name-style = 'eastern']" mode="inline-name"/>
    <xsl:apply-templates select="given-names" mode="inline-name"/>
    <xsl:apply-templates select="surname[not(../@name-style = 'eastern')]" mode="inline-name"/>
    <xsl:apply-templates select="suffix" mode="inline-name"/>
  </xsl:template>


  <xsl:template match="prefix" mode="inline-name">
    <xsl:apply-templates/>
    <xsl:if test="../surname | ../given-names | ../suffix">
      <xsl:text> </xsl:text>
    </xsl:if>
  </xsl:template>


  <xsl:template match="given-names" mode="inline-name">
    <xsl:apply-templates/>
    <xsl:if test="../surname[not(../@name-style = 'eastern')] | ../suffix">
      <xsl:text> </xsl:text>
    </xsl:if>
  </xsl:template>


  <xsl:template match="contrib/name/surname" mode="inline-name">
    <xsl:apply-templates/>
    <xsl:if test="../given-names[../@name-style = 'eastern'] | ../suffix">
      <xsl:text> </xsl:text>
    </xsl:if>
  </xsl:template>


  <xsl:template match="surname" mode="inline-name">
    <xsl:apply-templates/>
    <xsl:if test="../given-names[../@name-style = 'eastern'] | ../suffix">
      <xsl:text> </xsl:text>
    </xsl:if>
  </xsl:template>


  <xsl:template match="suffix" mode="inline-name">
    <xsl:apply-templates/>
  </xsl:template>


  <!-- string-name elements are written as is -->

  <xsl:template match="string-name">
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="string-name/*">
    <xsl:apply-templates/>
  </xsl:template>


  <!-- ============================================================= -->
  <!--  UTILITY TEMPLATES                                           -->
  <!-- ============================================================= -->


  <xsl:template name="append-pub-type">
    <!-- adds a value mapped for @pub-type, enclosed in parenthesis,
         to a string -->
    <xsl:for-each select="@pub-type">
      <xsl:text> (</xsl:text>
      <span class="data">
        <xsl:choose>
          <xsl:when test=". = 'epub'">electronic</xsl:when>
          <xsl:when test=". = 'ppub'">print</xsl:when>
          <xsl:when test=". = 'epub-ppub'">print and electronic</xsl:when>
          <xsl:when test=". = 'epreprint'">electronic preprint</xsl:when>
          <xsl:when test=". = 'ppreprint'">print preprint</xsl:when>
          <xsl:when test=". = 'ecorrected'">corrected, electronic</xsl:when>
          <xsl:when test=". = 'pcorrected'">corrected, print</xsl:when>
          <xsl:when test=". = 'eretracted'">retracted, electronic</xsl:when>
          <xsl:when test=". = 'pretracted'">retracted, print</xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="."/>
          </xsl:otherwise>
        </xsl:choose>
      </span>
      <xsl:text>)</xsl:text>
    </xsl:for-each>
  </xsl:template>


  <xsl:template name="metadata-labeled-entry">
    <xsl:param name="label"/>
    <xsl:param name="contents">
      <xsl:apply-templates/>
    </xsl:param>
    <xsl:call-template name="metadata-entry">
      <xsl:with-param name="contents">
        <xsl:if test="normalize-space(string($label))">
          <span class="generated">
            <xsl:copy-of select="$label"/>
            <xsl:text>: </xsl:text>
          </span>
        </xsl:if>
        <xsl:copy-of select="$contents"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


  <xsl:template name="metadata-entry">
    <xsl:param name="contents">
      <xsl:apply-templates/>
    </xsl:param>
    <p class="metadata-entry">
      <xsl:copy-of select="$contents"/>
    </p>
  </xsl:template>


  <xsl:template name="metadata-area">
    <xsl:param name="label"/>
    <xsl:param name="contents">
      <xsl:apply-templates/>
    </xsl:param>
    <div class="metadata-area">
      <xsl:if test="normalize-space(string($label))">
        <xsl:call-template name="metadata-labeled-entry">
          <xsl:with-param name="label">
            <xsl:copy-of select="$label"/>
          </xsl:with-param>
          <xsl:with-param name="contents"/>
        </xsl:call-template>
      </xsl:if>
      <div class="metadata-chunk">
        <xsl:copy-of select="$contents"/>
      </div>
    </div>
  </xsl:template>


  <xsl:template name="make-label-text">

    <xsl:apply-templates mode="label-text" select="label | @symbol"/>

  </xsl:template>


  <xsl:template name="assign-id">
    <xsl:variable name="id">
      <xsl:apply-templates select="." mode="id"/>
    </xsl:variable>
    <xsl:attribute name="id">
      <xsl:value-of select="$id"/>
    </xsl:attribute>
  </xsl:template>


  <xsl:template name="assign-src">
    <xsl:for-each select="@xlink:href">
      <xsl:attribute name="src">
        <xsl:value-of select="."/>
      </xsl:attribute>
    </xsl:for-each>
  </xsl:template>


  <xsl:template name="assign-href">
    <xsl:for-each select="@xlink:href">
      <xsl:attribute name="href">
        <xsl:value-of select="."/>
      </xsl:attribute>
    </xsl:for-each>
  </xsl:template>


  <xsl:template name="named-anchor">
    <!-- generates an HTML named anchor -->
    <xsl:variable name="id">
      <xsl:choose>
        <xsl:when test="@id">
          <!-- if we have an @id, we use it -->
          <xsl:value-of select="@id"/>
        </xsl:when>
        <xsl:when
          test="
            not(preceding-sibling::*) and
            (parent::alternatives | parent::name-alternatives |
            parent::citation-alternatives | parent::collab-alternatives |
            parent::aff-alternatives)/@id">
          <!-- if not, and we are first among our siblings inside one of
               several 'alternatives' wrappers, we use its @id if available -->
          <xsl:value-of
            select="
              (parent::alternatives | parent::name-alternatives |
              parent::citation-alternatives | parent::collab-alternatives |
              parent::aff-alternatives)/@id"
          />
        </xsl:when>
      </xsl:choose>
    </xsl:variable>
    <a id="{$id}"></a>
  </xsl:template>


  <!-- ============================================================= -->
  <!--  Process warnings                                             -->
  <!-- ============================================================= -->
  <!-- Generates a list of warnings to be reported due to processing
     anomalies. -->

  <xsl:template name="process-warnings">
    <!-- Only generate the warnings if set to $verbose -->
    <xsl:if test="$verbose">
      <!-- returns an RTF containing all the warnings -->
      <xsl:variable name="xref-warnings">
        <xsl:for-each select="//xref[not(normalize-space(string(.)))]">
          <xsl:variable name="target-label">
            <xsl:apply-templates select="key('element-by-id', @rid)" mode="label-text">
              <xsl:with-param name="warning" select="false()"/>
            </xsl:apply-templates>
          </xsl:variable>
          <xsl:if
            test="
              not(normalize-space(string($target-label))) and
              generate-id(.) = generate-id(key('xref-by-rid', @rid)[1])">
            <!-- if we failed to get a label with no warning, and
               this is the first cross-reference to this target
               we ask again to get the warning -->
            <li>
              <xsl:apply-templates select="key('element-by-id', @rid)" mode="label-text">
                <xsl:with-param name="warning" select="true()"/>
              </xsl:apply-templates>
            </li>
          </xsl:if>
        </xsl:for-each>
      </xsl:variable>

      <xsl:if test="normalize-space(string($xref-warnings))">
        <h4>Elements are cross-referenced without labels.</h4>
        <p>Either the element should be provided a label, or their cross-reference(s) should have
          literal text content.</p>
        <ul>
          <xsl:copy-of select="$xref-warnings"/>
        </ul>
      </xsl:if>


      <xsl:variable name="alternatives-warnings">
        <!-- for reporting any element with a @specific-use different
           from a sibling -->
        <xsl:for-each select="//*[@specific-use != ../*/@specific-use]/..">
          <li>
            <xsl:text>In </xsl:text>
            <xsl:apply-templates select="." mode="xpath"/>
            <ul>
              <xsl:for-each select="*[@specific-use != ../*/@specific-use]">
                <li>
                  <xsl:apply-templates select="." mode="pattern"/>
                </li>
              </xsl:for-each>
            </ul>
            <!--<xsl:text>: </xsl:text>
          <xsl:call-template name="list-elements">
            <xsl:with-param name="elements" select="*[@specific-use]"/>
          </xsl:call-template>-->
          </li>
        </xsl:for-each>
      </xsl:variable>

      <xsl:if test="normalize-space(string($alternatives-warnings))">
        <h4>Elements with different 'specific-use' assignments appearing together</h4>
        <ul>
          <xsl:copy-of select="$alternatives-warnings"/>
        </ul>
      </xsl:if>
    </xsl:if>

  </xsl:template>

  <!-- ============================================================= -->
  <!--  id mode                                                      -->
  <!-- ============================================================= -->
  <!-- An id can be derived for any element. If an @id is given,
     it is presumed unique and copied. If not, one is generated.   -->

  <xsl:template match="*" mode="id">
    <xsl:value-of select="@id"/>
    <xsl:if test="not(@id)">
      <xsl:value-of select="generate-id(.)"/>
    </xsl:if>
  </xsl:template>


  <xsl:template match="article | sub-article | response" mode="id">
    <xsl:value-of select="@id"/>
    <xsl:if test="not(@id)">
      <xsl:value-of select="local-name()"/>
      <xsl:number from="article" level="multiple" count="article | sub-article | response"
        format="1-1"/>
    </xsl:if>
  </xsl:template>


  <!-- ============================================================= -->
  <!--  "format-date"                                                -->
  <!-- ============================================================= -->
  <!-- Maps a structured date element to a string -->

  <xsl:template name="format-date">
    <!-- formats date in DD Month YYYY format -->
    <!-- context must be 'date', with content model:
         (((day?, month?) | season)?, year) -->
    <xsl:for-each select="day | month | season">
      <xsl:apply-templates select="." mode="map"/>
      <xsl:text> </xsl:text>
    </xsl:for-each>
    <xsl:apply-templates select="year" mode="map"/>
  </xsl:template>


  <xsl:template match="day | season | year" mode="map">
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="month" mode="map">
    <!-- maps numeric values to English months -->
    <xsl:choose>
      <xsl:when test="number() = 1">January</xsl:when>
      <xsl:when test="number() = 2">February</xsl:when>
      <xsl:when test="number() = 3">March</xsl:when>
      <xsl:when test="number() = 4">April</xsl:when>
      <xsl:when test="number() = 5">May</xsl:when>
      <xsl:when test="number() = 6">June</xsl:when>
      <xsl:when test="number() = 7">July</xsl:when>
      <xsl:when test="number() = 8">August</xsl:when>
      <xsl:when test="number() = 9">September</xsl:when>
      <xsl:when test="number() = 10">October</xsl:when>
      <xsl:when test="number() = 11">November</xsl:when>
      <xsl:when test="number() = 12">December</xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>




  <xsl:template match="*" mode="pattern">
    <xsl:value-of select="name(.)"/>
    <xsl:for-each select="@*">
      <xsl:text>[@</xsl:text>
      <xsl:value-of select="name()"/>
      <xsl:text>='</xsl:text>
      <xsl:value-of select="."/>
      <xsl:text>']</xsl:text>
    </xsl:for-each>
  </xsl:template>


  <xsl:template match="node()" mode="xpath">
    <xsl:apply-templates mode="xpath" select=".."/>
    <xsl:apply-templates mode="xpath-step" select="."/>
  </xsl:template>


  <xsl:template match="/" mode="xpath"/>


  <xsl:template match="*" mode="xpath-step">
    <xsl:variable name="name" select="name(.)"/>
    <xsl:text>/</xsl:text>
    <xsl:value-of select="$name"/>
    <xsl:if test="count(../*[name(.) = $name]) > 1">
      <xsl:text>[</xsl:text>
      <xsl:value-of select="count(. | preceding-sibling::*[name(.) = $name])"/>
      <xsl:text>]</xsl:text>
    </xsl:if>
  </xsl:template>


  <xsl:template match="@*" mode="xpath-step">
    <xsl:text>/@</xsl:text>
    <xsl:value-of select="name(.)"/>
  </xsl:template>


  <xsl:template match="comment()" mode="xpath-step">
    <xsl:text>/comment()</xsl:text>
    <xsl:if test="count(../comment()) > 1">
      <xsl:text>[</xsl:text>
      <xsl:value-of select="count(. | preceding-sibling::comment())"/>
      <xsl:text>]</xsl:text>
    </xsl:if>
  </xsl:template>


  <xsl:template match="processing-instruction()" mode="xpath-step">
    <xsl:text>/processing-instruction()</xsl:text>
    <xsl:if test="count(../processing-instruction()) > 1">
      <xsl:text>[</xsl:text>
      <xsl:value-of select="count(. | preceding-sibling::processing-instruction())"/>
      <xsl:text>]</xsl:text>
    </xsl:if>
  </xsl:template>


  <xsl:template match="text()" mode="xpath-step">
    <xsl:text>/text()</xsl:text>
    <xsl:if test="count(../text()) > 1">
      <xsl:text>[</xsl:text>
      <xsl:value-of select="count(. | preceding-sibling::text())"/>
      <xsl:text>]</xsl:text>
    </xsl:if>
  </xsl:template>


  <!-- ============================================================= -->
  <!--  End stylesheet                                               -->
  <!-- ============================================================= -->

</xsl:stylesheet>
